/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#ifndef STRAWBERRY_INTERNAL_VISIBILITY
#   define STRAWBERRY_INTERNAL_VISIBILITY private
#endif

#include <functional>
#include <optional>

#include "Source/Data/Database.hpp"
#include "Source/Data/Row.hpp"
#include "Source/Data/VariableList.hpp"
#include "Source/Data/TableViewPresentation.hpp"
#include "Source/Interface/MessageInterface.hpp"
#include "Source/Parser/Parser.hpp"

namespace strawberry {

    struct BiExpression;
    struct CreateTableQuery;
    struct DescribeQuery;
    struct InsertIntoQuery;
    struct LikeExpression;
    struct SelectFromQuery;
    struct ShowTablesQuery;

    class Strawberry {
    public:
        [[nodiscard]] explicit
        Strawberry(MessageInterface messageInterface);

        [[nodiscard]] Database *
        currentDatabase() const {
            return m_currentDatabase;
        }

        [[nodiscard]] std::vector<Database> &
        databases() {
            return m_databases;
        }

        [[nodiscard]] const std::vector<Database> &
        databases() const {
            return m_databases;
        }

        [[nodiscard]] Database *
        databaseByName(std::string_view name);

        [[nodiscard]] std::string_view
        getClosestDatabaseName(std::string_view name) const;

        [[nodiscard]] TableViewPresentation
        processQuery(std::string_view input);

        using SimpleRow = std::vector<Variable>;

    STRAWBERRY_INTERNAL_VISIBILITY:
        MessageInterface m_messageInterface;

        std::vector<Database> m_databases{};
        Database *m_currentDatabase{};

        Parser m_parser{m_messageInterface};

        enum struct SubroutineResult {
            INTERNAL_ERROR,
            ERROR,
            TRUE,
            FALSE,
        };

        enum struct AggregationByProductInclude {
            Yes,
            No
        };

        struct AggregatedRowsResult {
            std::vector<ColumnSpecifier> specifiers{};
            std::vector<SimpleRow> rows;

            std::vector<ColumnSpecifier> byProductSpecifiers{};
            std::vector<SimpleRow> byProductRows;
        };

        [[nodiscard]] static AggregatedRowsResult
        aggregateRows(const std::map<std::string_view, Table *> &tables, const SelectFromQuery *query,
                      AggregationByProductInclude includeByProducts);

        // The function returns std::nullopt when the expression couldn't be
        // evaluated.
        [[nodiscard]] std::optional<Variable>
        evaluateExpression(const Expression *expression, const VariableList &variableList) const;

        [[nodiscard]] std::optional<bool>
        evaluateComparisonExpression(const BiExpression *expression, const VariableList &variableList) const;

        [[nodiscard]] std::optional<bool>
        evaluateLikeExpression(const LikeExpression *expression, const VariableList &variableList) const;

        [[nodiscard]] SubroutineResult
        invokeWhereClauseOnRow(const Row &row, const Row &byProductRow, const Expression *expression) const;

        inline void
        postQueryError(std::string_view sv) const {
            if (m_messageInterface.onQueryError)
                m_messageInterface.onQueryError(sv);
        }

        inline void
        postInformationalMessage(std::string_view sv) const {
            if (m_messageInterface.onInformation)
                m_messageInterface.onInformation(sv);
        }

        [[nodiscard]] TableViewPresentation
        processCreateTableQuery(CreateTableQuery *query);

        [[nodiscard]] TableViewPresentation
        processDescribeQuery(DescribeQuery *query);

        [[nodiscard]] TableViewPresentation
        processInsertIntoQuery(InsertIntoQuery *query);

        [[nodiscard]] TableViewPresentation
        processSelectFromQuery(SelectFromQuery *query);

        [[nodiscard]] TableViewPresentation
        processShowDatabasesQuery();

        [[nodiscard]] TableViewPresentation
        processShowTablesQuery(ShowTablesQuery *query);
    };

} // namespace strawberry
