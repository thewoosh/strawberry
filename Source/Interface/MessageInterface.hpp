/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <functional>
#include <string>

namespace strawberry {

    struct MessageInterface {
        /**
         * This function will create an MI which won't display informational
         * messages, but will act fatally and display the message when an error
         * is delivered.
         */
        [[nodiscard]] static MessageInterface
        createFatalError(const std::function<void()> &callback = {});

        /**
         * This function will create an MI which won't do anything when a message
         * is delivered.
         */
        [[nodiscard]] inline static MessageInterface
        createSilentNull() {
            return {};
        }

        /**
         * This function will create an MI which will print all messages to
         * the stdout file.
         */
        [[nodiscard]] static MessageInterface
        createStdout();

        // When the query contains an invalid token.
        // Example: Invalid character in query: 0x7e or '~'
        std::function<void (std::string_view)> onLexerError{};

        // When a query doesn't follow the correct (or any) structure.
        // Example: "INSERT" should be followed by "INTO"
        std::function<void (std::string_view)> onParserError{};

        // When a query is syntactically correct, but its logic doesn't make sense
        // Example: table "invalid_table" does not exist.
        std::function<void (std::string_view)> onQueryError{};

        // When information about a query has been published.
        // Example: Inserted 2 rows in table "test";
        std::function<void (std::string_view)> onInformation{};
    };

} // namespace strawberry
