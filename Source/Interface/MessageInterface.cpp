/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/Interface/MessageInterface.hpp"

#include <exception> // for std::terminate()

#include <csignal>

#include <fmt/core.h>

namespace strawberry {

    MessageInterface
    MessageInterface::createFatalError(const std::function<void()> &inCallback) {
        MessageInterface messageInterface;

        const std::function<void()> &fatalResponse = inCallback ? inCallback : [] () {
            fmt::print("\nConsidering previous error fatally.\nTerminating...\n");
            std::terminate();
        };

        messageInterface.onLexerError = [fatalResponse] (std::string_view message) {
            fmt::print("Lexer Error: {}\n", message);
            fatalResponse();
        };

        messageInterface.onParserError = [fatalResponse] (std::string_view message) {
            fmt::print("Parser Error: {}\n", message);
            fatalResponse();
        };

        messageInterface.onQueryError = [fatalResponse] (std::string_view message) {
            fmt::print("Query Error: {}\n", message);
            fatalResponse();
        };

        return messageInterface;
    }

    MessageInterface
    MessageInterface::createStdout() {
        MessageInterface messageInterface;

        messageInterface.onLexerError = [] (std::string_view message) {
            fmt::print("Lexer Error: {}\n", message);
        };

        messageInterface.onParserError = [] (std::string_view message) {
            fmt::print("Parser Error: {}\n", message);
        };

        messageInterface.onQueryError = [] (std::string_view message) {
            fmt::print("Query Error: {}\n", message);
        };

        messageInterface.onInformation = [] (std::string_view message) {
            fmt::print("{}\n", message);
        };

        return messageInterface;
    }

} // namespace strawberry
