/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <functional>
#include <memory>
#include <optional>
#include <string_view>

#include "Source/Data/Variable.hpp"
#include "Source/Interface/MessageInterface.hpp"
#include "Source/Syntax/Query.hpp"

namespace strawberry {

    struct Expression;
    struct Token;

    using TokenIterator = std::vector<Token>::iterator;

    class Parser {
    public:
        [[nodiscard]] inline explicit
        Parser(MessageInterface &messageInterface)
                : m_messageInterface(messageInterface) {
        }

        [[nodiscard]] std::unique_ptr<Query>
        parseQuery(std::string_view input);

    private:
        MessageInterface &m_messageInterface;

        inline void
        onError(std::string_view message) const {
            if (m_messageInterface.onParserError)
                m_messageInterface.onParserError(message);
        }

        //
        // Expressions
        //

        [[nodiscard]] std::unique_ptr<Expression>
        parseCompareExpression(std::vector<Token> &tokens, TokenIterator &iterator);

        /**
         * The main entrypoint for all expressions.
         */
        [[nodiscard]] std::unique_ptr<Expression>
        parseExpression(std::vector<Token> &tokens, TokenIterator &iterator);

        [[nodiscard]] std::unique_ptr<Expression>
        parsePrimaryExpression(std::vector<Token> &tokens, TokenIterator &iterator);

        //
        // Queries
        //

        [[nodiscard]] std::unique_ptr<Query>
        parseCreateQuery(std::vector<Token> &&tokens, TokenIterator iterator);

        [[nodiscard]] std::unique_ptr<Query>
        parseCreateTableQuery(std::vector<Token> &&tokens, TokenIterator iterator);

        [[nodiscard]] std::unique_ptr<Query>
        parseDescribeQuery(std::vector<Token> &&tokens, TokenIterator iterator);

        [[nodiscard]] std::unique_ptr<Query>
        parseInsertQuery(std::vector<Token> &&tokens, TokenIterator iterator);

        [[nodiscard]] std::unique_ptr<Query>
        parseSelectQuery(std::vector<Token> &&tokens, TokenIterator iterator);

        [[nodiscard]] std::unique_ptr<Query>
        parseShowQuery(std::vector<Token> &&tokens, TokenIterator iterator);

        [[nodiscard]] std::unique_ptr<Query>
        parseShowDatabasesQuery(std::vector<Token> &&tokens, TokenIterator iterator, ReservedIdentifier showTypeKeyword);

        [[nodiscard]] std::unique_ptr<Query>
        parseShowTablesQuery(std::vector<Token> &&tokens, TokenIterator iterator);

        //
        // Other
        //

        [[nodiscard]] std::optional<Variable>
        parseVariable(std::vector<Token> &tokens, TokenIterator &iterator);
    };

} // namespace strawberry
