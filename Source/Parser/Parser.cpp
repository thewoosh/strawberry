/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Parser.hpp"

#include <fmt/format.h>

#include "Source/Parser/Lexer.hpp"
#include "Source/Parser/Token.hpp"
#include "Source/Syntax/CreateTableQuery.hpp"
#include "Source/Syntax/DescribeQuery.hpp"
#include "Source/Syntax/InsertIntoQuery.hpp"
#include "Source/Syntax/SelectFromQuery.hpp"
#include "Source/Syntax/ShowTablesQuery.hpp"

namespace strawberry {

    std::unique_ptr<Query>
    Parser::parseQuery(std::string_view input) {
        auto possibleTokens = runLexer(input, m_messageInterface.onLexerError);
        if (!possibleTokens.has_value())
            return nullptr;

        auto tokens = std::move(possibleTokens.value());
        if (std::empty(tokens))
            return nullptr;

        if (tokens.front().type() != Token::Type::RESERVED_IDENTIFIER) {
            onError("Query doesn't start with a reserved identifier (e.g. CREATE, INSERT, SELECT, etc.)");
            return nullptr;
        }

        const auto reservedIdentifier = tokens.front().asReservedIdentifier();
        if (reservedIdentifier == ReservedIdentifier::CREATE) {
            return parseCreateQuery(std::move(tokens), std::next(std::begin(tokens)));
        }

        if (reservedIdentifier == ReservedIdentifier::DESCRIBE) {
            return parseDescribeQuery(std::move(tokens), std::next(std::begin(tokens)));
        }

        if (reservedIdentifier == ReservedIdentifier::INSERT) {
            return parseInsertQuery(std::move(tokens), std::next(std::begin(tokens)));
        }

        if (reservedIdentifier == ReservedIdentifier::SELECT) {
            return parseSelectQuery(std::move(tokens), std::next(std::begin(tokens)));
        }

        if (reservedIdentifier == ReservedIdentifier::SHOW) {
            return parseShowQuery(std::move(tokens), std::next(std::begin(tokens)));
        }

        onError(fmt::format("\"{}\" is an unknown query beginning", toString(reservedIdentifier)));
        return nullptr;
    }

    std::unique_ptr<Query>
    Parser::parseCreateQuery(std::vector<Token> &&tokens, TokenIterator iterator) {
        if (iterator == std::end(tokens) || iterator->type() != Token::Type::RESERVED_IDENTIFIER) {
            onError(R"("CREATE" should be followed by a keyword, e.g. "TABLE")");
            return nullptr;
        }

        const auto reservedIdentifierIndicatingWhatToCreate = iterator->asReservedIdentifier();
        ++iterator;

        if (reservedIdentifierIndicatingWhatToCreate == ReservedIdentifier::TABLE) {
            return parseCreateTableQuery(std::move(tokens), iterator);
        }

        onError(fmt::format(R"(Unknown type to "CREATE": "{}", expected one of: "TABLE")",
                            toString(reservedIdentifierIndicatingWhatToCreate)));
        return nullptr;
    }

    std::unique_ptr<Query>
    Parser::parseCreateTableQuery(std::vector<Token> &&tokens, TokenIterator iterator) {
        if (iterator == std::end(tokens) || iterator->type() != Token::Type::IDENTIFIER) {
            onError(R"("CREATE TABLE" should be followed by a table name)");
            return nullptr;
        }

        auto query = std::make_unique<CreateTableQuery>(std::move(iterator->asString()));
        ++iterator;

        if (iterator == std::end(tokens) || iterator->type() != Token::Type::LEFT_PARENTHESIS) {
            onError(fmt::format(R"("CREATE TABLE {}" should be followed by a '(')", query->tableName()));
            return nullptr;
        }

        ++iterator;
        while (true) {
            if (iterator == std::end(tokens)) {
                if (query->columns().empty())
                    onError("Unexpected end of query; expected ')' or a column name");
                else
                    onError("Unexpected end of query; expected a column name");
                return nullptr;
            }

            if (iterator->type() == Token::Type::RIGHT_PARENTHESIS) {
                onError("Unexpected ')'; a table should have at least one column!");
                return nullptr;
            }

            if (iterator->type() != Token::Type::IDENTIFIER) {
                onError("Unexpected token; expected ')' or a column name");
                return nullptr;
            }

            auto columnName = std::move(iterator->asString());

            if (++iterator == std::end(tokens) || iterator->type() != Token::Type::RESERVED_IDENTIFIER) {
                onError("Unexpected token; expected type qualifier after column name");
                return nullptr;
            }

            auto possibleTypeName = convertReservedIdentifierToTypeName(iterator->asReservedIdentifier());
            ++iterator;

            if (!possibleTypeName.has_value()) {
                onError(fmt::format("Invalid type name: \"{}\", expected one of: {}", toString(iterator->asReservedIdentifier()),
                                    g_typeNameCombinedAsString.substr(0, g_typeNameCombinedAsString.length() - 2)));
                return nullptr;
            }

            const auto columnTypeName = possibleTypeName.value();
            std::size_t columnTypeLength = 0;

            if (doesTypeNameTakeLength(columnTypeName)) {
                if (std::distance(iterator, std::end(tokens)) < 3) {
                    onError("Unexpected end of query; expected length of type name, e.g. \"(5)\"");
                    return nullptr;
                }

                if (iterator->type() != Token::Type::LEFT_PARENTHESIS) {
                    onError("Unexpected token; expected '(' as beginning of type length, e.g. \"(5)\"");
                    return nullptr;
                }

                ++iterator;
                if (iterator->type() != Token::Type::INTEGER) {
                    onError("Unexpected token; expected a number as the type length");
                    return nullptr;
                }

                if (iterator->asInt() < 0) {
                    onError(fmt::format("The type length of a column cannot be negative: {}", iterator->asInt()));
                    return nullptr;
                }

                columnTypeLength = static_cast<std::size_t>(iterator->asInt());
                ++iterator;

                if (iterator->type() != Token::Type::RIGHT_PARENTHESIS) {
                    onError("Unexpected token; expected ')' as end of type length, e.g. \"(5)\"");
                    return nullptr;
                }
            }

            query->columns().emplace_back(std::move(columnName), TypeSpecifier(columnTypeName, columnTypeLength));

            if (iterator->type() == Token::Type::RIGHT_PARENTHESIS) {
                break;
            }

            if (iterator->type() == Token::Type::COMMA) {
                ++iterator;
                continue;
            }

            onError("Unexpected token; expected ')' or ',' after column");
            return nullptr;
        }

        if (query->columns().empty()) {
            onError("A table should have at least one column!");
            return nullptr;
        }

        return query;
    }

    std::unique_ptr<Query>
    Parser::parseDescribeQuery(std::vector<Token> &&tokens, TokenIterator iterator) {
        if (iterator == std::end(tokens) || iterator->type() != Token::Type::IDENTIFIER) {
            onError("\"DESCRIBE\" should be followed by a table name");
            return nullptr;
        }

        return std::make_unique<DescribeQuery>(std::move(iterator->asString()));
    }

    std::unique_ptr<Query>
    Parser::parseInsertQuery(std::vector<Token> &&tokens, TokenIterator iterator) {
        if (iterator == std::end(tokens) || iterator->type() != Token::Type::RESERVED_IDENTIFIER
                || iterator->asReservedIdentifier() != ReservedIdentifier::INTO) {
            onError(R"("INSERT" should be followed by "INTO")");
            return nullptr;
        }

        ++iterator;
        if (iterator == std::end(tokens) || iterator->type() != Token::Type::IDENTIFIER) {
            onError("\"INSERT INTO\" should be followed by a table name identifier");
            return nullptr;
        }

        auto tableName = std::move(iterator->asString());
        ++iterator;

        if (iterator == std::end(tokens)) {
            onError(fmt::format(R"(Unexpected end of query; "INSERT INTO {}" should be followed by a '(' or "VALUES")", tableName));
            return nullptr;
        }

        auto query = std::make_unique<InsertIntoQuery>(std::move(tableName));

        if (iterator->type() == Token::Type::RESERVED_IDENTIFIER && iterator->asReservedIdentifier() == ReservedIdentifier::VALUES) {
            ++iterator;

            while (true) {
                if (iterator == std::end(tokens) || iterator->type() != Token::Type::LEFT_PARENTHESIS) {
                    onError("Expected '(' in VALUES clause");
                    return nullptr;
                }

                ++iterator;
                std::vector<Variable> variables{};

                while (true) {
                    if (iterator == std::end(tokens)) {
                        onError("Unexpected end of query; expected a variable in VALUES clause");
                        return nullptr;
                    }

                    auto possibleVariable = parseVariable(tokens, iterator);
                    if (!possibleVariable.has_value()) {
                        onError("Expected variable in VALUES clause");
                        return nullptr;
                    }

                    variables.push_back(std::move(possibleVariable.value()));

                    if (iterator == std::end(tokens)) {
                        onError("Unexpected end of query; expected ',' or ')' after variable");
                        return nullptr;
                    }

                    if (iterator->type() == Token::Type::COMMA) {
                        ++iterator;
                        continue;
                    }

                    if (iterator->type() == Token::Type::RIGHT_PARENTHESIS) {
                        ++iterator;
                        break;
                    }

                    onError("Unexpected token; expected ',' or ')' after variable");
                    return nullptr;
                }

                query->values().push_back(std::move(variables));

                if (iterator == std::end(tokens)) {
                    onError("Unexpected end of query; expected ',' or ';' after VALUES in INSERT INTO query");
                    return nullptr;
                }

                if (iterator->type() == Token::Type::COMMA) {
                    ++iterator;
                    continue;
                }

                if (iterator->type() == Token::Type::SEMICOLON) {
                    ++iterator;
                    break;
                }

                onError("Unexpected token; expected ',' or ';' after VALUES in INSERT INTO query");
                return nullptr;
            }

            return query;
        }

        onError("Unexpected token, expected '(' or VALUES");
        return nullptr;
    }

    std::unique_ptr<Query>
    Parser::parseSelectQuery(std::vector<Token> &&tokens, TokenIterator iterator) {
        auto query = std::make_unique<SelectFromQuery>();

        // Parse columns
        while (true) {
            if (iterator == std::end(tokens)) {
                onError("Unexpected end of query; expected one or more columns");
            }

            if (iterator->type() == Token::Type::ASTERISK) {
                if (!query->columns().empty()) {
                    onError("Unexpected '*'; a loose '*' is only allowed as the single column selector");
                    return nullptr;
                }

                query->columns().emplace_back(TableColumnSpecifier{"", "*"});

                ++iterator;
                if (iterator == std::end(tokens) || iterator->type() != Token::Type::RESERVED_IDENTIFIER
                        || iterator->asReservedIdentifier() != ReservedIdentifier::FROM) {
                    onError("Unexpected token; expected \"FROM\" after '*'");
                    return nullptr;
                }

                ++iterator;
                break;
            }

            if (iterator->type() != Token::Type::IDENTIFIER) {
                onError("Expected identifier as column selector in SELECT statement");
                return nullptr;
            }

            auto column = std::move(iterator->asString());
            ++iterator;

            // TODO support table selectors; `table`.`column` instead of just `column`
            std::string specificTableName{};

            bool wasAlias = true;
            if (iterator != std::end(tokens) && iterator->type() == Token::Type::RESERVED_IDENTIFIER
                && iterator->asReservedIdentifier() == ReservedIdentifier::AS) {
                ++iterator; // consume 'AS' keyword
                if (iterator == std::end(tokens) || iterator->type() != Token::Type::IDENTIFIER) {
                    onError("Expected identifier as alias name for table selector");
                    return nullptr;
                }

                query->columns().emplace_back(TableColumnSpecifier{std::move(specificTableName), std::move(column)},
                                              std::move(iterator->asString()));
                ++iterator;
            } else if (iterator != std::end(tokens) && iterator->type() == Token::Type::IDENTIFIER) {
                query->columns().emplace_back(TableColumnSpecifier{std::move(specificTableName), std::move(column)},
                                              std::move(iterator->asString()));
                ++iterator;
            } else {
                wasAlias = false;
                query->columns().emplace_back(TableColumnSpecifier{std::move(specificTableName), std::move(column)});
            }

            if (iterator != std::end(tokens)) {
                if (iterator->type() == Token::Type::COMMA) {
                    ++iterator;
                    continue;
                }

                if (iterator->type() == Token::Type::RESERVED_IDENTIFIER
                        && iterator->asReservedIdentifier() == ReservedIdentifier::FROM) {
                    ++iterator;
                    break;
                }
            }

            onError("Unexpected token; expected ',' or \"FROM\" after column selector");

            if (wasAlias) {
                onError("Unexpected token; expected ',' or \"FROM\" after column selector");
            } else {
                onError(R"(Unexpected token; expected an identifier or "AS" as alias name, or a ',', or "FROM" after column selector)");
            }

            return nullptr;
        }

        // Parse tables
        // Parse columns
        while (true) {
            if (iterator == std::end(tokens)) {
                onError("Unexpected end of query; expected one or more tables");
                return nullptr;
            }

            if (iterator->type() != Token::Type::IDENTIFIER) {
                onError("Expected identifier as table selector in SELECT statement");
                return nullptr;
            }

            auto table = std::move(iterator->asString());
            ++iterator;

            bool wasAlias = true;
            if (iterator != std::end(tokens) && iterator->type() == Token::Type::RESERVED_IDENTIFIER
                    && iterator->asReservedIdentifier() == ReservedIdentifier::AS) {
                ++iterator; // consume 'AS' keyword
                if (iterator == std::end(tokens) || iterator->type() != Token::Type::IDENTIFIER) {
                    onError("Expected identifier as alias name for table selector");
                    return nullptr;
                }

                query->tables().emplace_back(std::move(table), std::move(iterator->asString()));
                ++iterator;
            } else if (iterator != std::end(tokens) && iterator->type() == Token::Type::IDENTIFIER) {
                query->tables().emplace_back(std::move(table), std::move(iterator->asString()));
                ++iterator;
            } else {
                wasAlias = false;
                query->tables().emplace_back(std::move(table));
            }

            if (iterator != std::end(tokens)) {
                if (iterator->type() == Token::Type::COMMA) {
                    ++iterator;
                    continue;
                }

                if (iterator->type() == Token::Type::SEMICOLON) {
                    ++iterator;
                    return query;
                }

                if (iterator->type() == Token::Type::RESERVED_IDENTIFIER)
                    break;
            }

            if (wasAlias) {
                onError("Unexpected token; expected ',' or ';' after table selector");
            } else {
                onError("Unexpected token; expected an identifier, \"AS\", ',' or ';' after table selector");
            }

            return nullptr;
        }

        if (iterator->type() == Token::Type::SEMICOLON) {
            ++iterator;
            return query;
        }

        if (iterator->type() != Token::Type::RESERVED_IDENTIFIER) {
            onError("Unexpected token; expected a reserved identifier");
            return nullptr;
        }

        if (iterator->asReservedIdentifier() == ReservedIdentifier::WHERE) {
            ++iterator;
            auto expression = parseExpression(tokens, iterator);
            if (!expression) {
                onError("Failed to produce an expression for the WHERE clause");
                return nullptr;
            }
            query->whereExpression() = std::move(expression);
        }

        if (iterator != std::end(tokens) && iterator->type() == Token::Type::RESERVED_IDENTIFIER &&
                iterator->asReservedIdentifier() == ReservedIdentifier::ORDER) {
            ++iterator; // consume ReservedIdentifier ORDER

            if (iterator == std::end(tokens) || iterator->type() != Token::Type::RESERVED_IDENTIFIER
                    || iterator->asReservedIdentifier() != ReservedIdentifier::BY) {
                onError("Unexpected token; expected 'BY' after 'ORDER' to complete the ORDER BY clause start");
                return nullptr;
            }

            ++iterator; // consume ReservedIdentifier BY

            while (true) {
                if (iterator == std::end(tokens) || iterator->type() != Token::Type::IDENTIFIER) {
                    onError("Unexpected token; expected an identifier for the column or table name to ORDER BY");
                    return nullptr;
                }

                std::string columnName = std::move(iterator->asString());
                ++iterator;
                if (iterator == std::end(tokens)) {
                    onError("Unexpected end of query after identifier for column name");
                    return nullptr;
                }

                OrderBySpecifier::Type orderByType;
                if (iterator->type() == Token::Type::RESERVED_IDENTIFIER
                        && iterator->asReservedIdentifier() == ReservedIdentifier::ASC) {
                    orderByType = OrderBySpecifier::Type::ASCENDING;
                    ++iterator;
                } else if (iterator->type() == Token::Type::RESERVED_IDENTIFIER
                       && iterator->asReservedIdentifier() == ReservedIdentifier::DESC) {
                    orderByType = OrderBySpecifier::Type::DESCENDING;
                    ++iterator;
                } else {
                    orderByType = OrderBySpecifier::Type::ASCENDING;
                }

                // TODO support table selectors; `table`.`column` instead of just `column`

                query->orderBySpecifiers().emplace_back(orderByType, TableColumnSpecifier{std::string{}, std::move(columnName)});

                if (iterator == std::end(tokens) || iterator->type() != Token::Type::COMMA)
                    break;
                ++iterator; // consume a , U+002C COMMA
            }
        }

        if (iterator != std::end(tokens) && iterator->type() == Token::Type::SEMICOLON) {
            ++iterator;
            return query;
        }

        onError("Unexpected token; expected ';'");
        return nullptr;
    }

    std::unique_ptr<Query>
    Parser::parseShowQuery(std::vector<Token> &&tokens, TokenIterator iterator) {
        if (iterator == std::end(tokens) || iterator->type() != Token::Type::RESERVED_IDENTIFIER) {
            onError(R"("SHOW" should be followed by a type keyword, e.g. "DATABASES", "TABLES")");
            return nullptr;
        }

        const auto reservedIdentifierIndicatingWhatToShow = iterator->asReservedIdentifier();
        ++iterator;

        if (reservedIdentifierIndicatingWhatToShow == ReservedIdentifier::DATABASES
                || reservedIdentifierIndicatingWhatToShow == ReservedIdentifier::SCHEMAS) {
            return parseShowDatabasesQuery(std::move(tokens), iterator, reservedIdentifierIndicatingWhatToShow);
        }

        if (reservedIdentifierIndicatingWhatToShow == ReservedIdentifier::TABLES) {
            return parseShowTablesQuery(std::move(tokens), iterator);
        }

        onError(fmt::format(R"(Unknown type to "CREATE": "{}", expected one of: "TABLE")",
                            toString(reservedIdentifierIndicatingWhatToShow)));
        return nullptr;
    }

    std::unique_ptr<Query>
    Parser::parseShowDatabasesQuery(std::vector<Token> &&tokens, TokenIterator iterator,
                                    ReservedIdentifier showTypeKeyword) {
        if (iterator == std::end(tokens) || iterator->type() != Token::Type::SEMICOLON) {
            if (showTypeKeyword == ReservedIdentifier::DATABASES)
                onError("Unexpected token; expected ';' after \"SHOW DATABASES\"");
            else if (showTypeKeyword == ReservedIdentifier::SCHEMAS)
                onError("Unexpected token; expected ';' after \"SHOW SCHEMAS\"");
            return nullptr;
        }

        return std::make_unique<Query>(Query::Type::SHOW_DATABASES);
    }

    std::unique_ptr<Query>
    Parser::parseShowTablesQuery(std::vector<Token> &&tokens, TokenIterator iterator) {
        std::string databaseName{};
        if (iterator != std::end(tokens) && iterator->type() == Token::Type::RESERVED_IDENTIFIER
            && (iterator->asReservedIdentifier() == ReservedIdentifier::FROM
                || iterator->asReservedIdentifier() == ReservedIdentifier::IN)) {
            ++iterator;

            if (iterator == std::end(tokens) || iterator->type() != Token::Type::IDENTIFIER) {
                onError("Unexpected token; expected an identifier for the name of the database");
                return nullptr;
            }

            databaseName = std::move(iterator->asString());
            ++iterator;

            if (iterator == std::end(tokens) || iterator->type() != Token::Type::SEMICOLON) {
                onError("Unexpected token; expected ';' after the name of the database");
                return nullptr;
            }
        } else if (iterator == std::end(tokens) || iterator->type() != Token::Type::SEMICOLON) {
            onError("Unexpected token; expected ';' after \"SHOW TABLES\"");
            return nullptr;
        }

        return std::make_unique<ShowTablesQuery>(std::move(databaseName));
    }

    std::optional<Variable>
    Parser::parseVariable(std::vector<Token> &tokens, TokenIterator &iterator) {
        if (iterator == std::end(tokens)) {
            onError("Unexpected end of query; expected a variable");
            return {};
        }

        if (iterator->type() == Token::Type::INTEGER) {
            return Variable{iterator++->asInt()};
        }

        if (iterator->type() == Token::Type::STRING) {
            return Variable{std::move(iterator++->asString())};
        }

        onError("Unexpected token; expected a variable");
        return {};
    }

} // namespace strawberry
