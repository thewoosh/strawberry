/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Parser.hpp"

#include "Source/Expression/BiExpression.hpp"
#include "Source/Expression/LikeExpression.hpp"
#include "Source/Expression/PrimaryExpression.hpp"
#include "Source/Parser/Token.hpp"
#include "Source/Utility/Cast.hpp"

namespace strawberry {

    [[nodiscard]] std::optional<Expression::Type>
    convertTokenToComparisonOperator(const Token &token) {
        switch (token.type()) {
            case Token::Type::EQUAL_TO_SIGN:
                return Expression::Type::COMPARE_EQUAL_TO;
            case Token::Type::GREATER_THAN_SIGN:
                return Expression::Type::COMPARE_GREATER_THAN;
            case Token::Type::GREATER_THAN_OR_EQUAL_TO_SIGN:
                return Expression::Type::COMPARE_GREATER_THAN_OR_EQUAL;
            case Token::Type::LESS_THAN_SIGN:
                return Expression::Type::COMPARE_LESS_THAN;
            case Token::Type::LESS_THAN_OR_EQUAL_TO_SIGN:
                return Expression::Type::COMPARE_LESS_THAN_OR_EQUAL;
            case Token::Type::NOT_EQUAL_TO_SIGN:
                return Expression::Type::COMPARE_NOT_EQUAL_TO;
            default:
                return std::nullopt;
        }
    }

    std::unique_ptr<Expression>
    Parser::parseCompareExpression(std::vector<Token> &tokens, TokenIterator &iterator) {
        auto expression = parsePrimaryExpression(tokens, iterator);
        if (expression == nullptr || iterator == std::end(tokens))
            return expression;

        if (expression->type() == Expression::Type::PRIMARY_IDENTIFIER
                && iterator->type() == Token::Type::RESERVED_IDENTIFIER
                && iterator->asReservedIdentifier() == ReservedIdentifier::LIKE) {
            ++iterator;

            if (iterator == std::end(tokens) || iterator->type() != Token::Type::STRING) {
                onError("Keyword \"LIKE\" isn't followed by a string");
                return nullptr;
            }

            return std::make_unique<LikeExpression>(
                    TableColumnSpecifier{"", std::move(downcast<PrimaryExpression<std::string> *>(expression.get())->data())},
                    std::move(iterator++->asString())
            );
        }

        auto possibleType = convertTokenToComparisonOperator(*iterator);
        if (!possibleType.has_value())
            return expression;

        ++iterator;

        auto rhs = parsePrimaryExpression(tokens, iterator);
        if (!rhs) {
            onError("failed to parse expression for right hand side of comparison-expression");
            return nullptr;
        }

        return std::make_unique<BiExpression>(possibleType.value(), std::move(expression), std::move(rhs));
    }

    std::unique_ptr<Expression>
    Parser::parseExpression(std::vector<Token> &tokens, TokenIterator &iterator) {
        return parseCompareExpression(tokens, iterator);
    }

    std::unique_ptr<Expression>
    Parser::parsePrimaryExpression(std::vector<Token> &tokens, TokenIterator &iterator) {
        if (iterator == std::end(tokens)) {
            onError("unexpected end of query, expected a primary-expression");
            return nullptr;
        }

        if (iterator->type() == Token::Type::INTEGER) {
            auto value = iterator->asInt();
            ++iterator;
            return std::make_unique<PrimaryExpression<std::int64_t>>(Expression::Type::PRIMARY_INTEGER, value);
        }

        if (iterator->type() == Token::Type::IDENTIFIER) {
            auto value = std::move(iterator->asString());
            ++iterator;
            return std::make_unique<PrimaryExpression<std::string>>(Expression::Type::PRIMARY_IDENTIFIER, std::move(value));
        }

        onError("unexpected token, expected a primary-expression (e.g. integer)");
        return nullptr;
    }

} // namespace strawberry
