/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <functional>
#include <optional>
#include <string_view>
#include <vector>

namespace strawberry {

    struct Token;

    using LexerErrorReporterType = std::function<void(std::string_view)>;

    [[nodiscard]] std::optional<std::vector<Token>>
    runLexer(std::string_view input, const LexerErrorReporterType &errorReporter);

} // namespace strawberry
