/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string>
#include <variant>

#include <cstdint>

#include "Source/Syntax/ReservedIdentifier.hpp"

namespace strawberry {

    struct Token {
        enum class Type {
            ASTERISK,
            COMMA,
            IDENTIFIER,
            INTEGER,
            LEFT_PARENTHESIS,
            RESERVED_IDENTIFIER,
            RIGHT_PARENTHESIS,
            SEMICOLON,
            STRING,

            // Operators
            EQUAL_TO_SIGN,
            GREATER_THAN_SIGN,
            GREATER_THAN_OR_EQUAL_TO_SIGN,
            LESS_THAN_SIGN,
            LESS_THAN_OR_EQUAL_TO_SIGN,
            NOT_EQUAL_TO_SIGN,
        };

        [[nodiscard]] inline explicit
        Token(Type type)
                : m_type(type) {
        }

        template <typename Contained>
        [[nodiscard]] inline
        Token(Type type, Contained contained)
                : m_type(type)
                , m_data(std::move(contained)) {
        }

        [[nodiscard]] inline static Token
        create(Type type) {
            return Token{type};
        }

        [[nodiscard]] inline static Token
        createIdentifier(std::string &&string) {
            return Token{Type::IDENTIFIER, std::move(string)};
        }

        [[nodiscard]] inline static Token
        createInt(std::int64_t integer) {
            return Token{Type::INTEGER, integer};
        }

        [[nodiscard]] inline static Token
        createReservedIdentifier(ReservedIdentifier reservedIdentifier) {
            return Token{Type::RESERVED_IDENTIFIER, reservedIdentifier};
        }

        [[nodiscard]] inline static Token
        createString(std::string &&string) {
            return Token{Type::STRING, std::move(string)};
        }

        [[nodiscard]] inline constexpr Type
        type() const {
            return m_type;
        }

        [[nodiscard]] inline std::int64_t
        asInt() const {
            return std::get<std::int64_t>(m_data);
        }

        [[nodiscard]] inline ReservedIdentifier
        asReservedIdentifier() const {
            return std::get<ReservedIdentifier>(m_data);
        }

        [[nodiscard]] inline std::string &
        asString() {
            return std::get<std::string>(m_data);
        }

        [[nodiscard]] inline const std::string &
        asString() const {
            return std::get<std::string>(m_data);
        }

    private:
        const Type m_type;
        std::variant<std::string, std::int64_t, ReservedIdentifier> m_data{};
    };

} // namespace strawberry
