/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/Parser/Lexer.hpp"

#include <cstdlib> // for std::strtoll

#include <fmt/printf.h>

#include "Source/Parser/Token.hpp"
#include "Source/Utility/Characters.hpp"
#include "Source/Utility/String.hpp"

namespace strawberry {

    std::optional<std::vector<Token>>
    runLexer(std::string_view input, const LexerErrorReporterType &errorReporter) {
        std::vector<Token> tokens{};
        auto it = std::begin(input);

        while (it != std::end(input)) {
            switch (*it++) {
                case ' ':
                case '\n':
                case '\t':
                case '\f':
                case '\v':
                    continue;
                case '!':
                    if (it == std::end(input) || *it != '=')
                        break;
                    ++it;
                    tokens.push_back(Token::create(Token::Type::NOT_EQUAL_TO_SIGN));
                    continue;
                case '(':
                    tokens.push_back(Token::create(Token::Type::LEFT_PARENTHESIS));
                    continue;
                case ')':
                    tokens.push_back(Token::create(Token::Type::RIGHT_PARENTHESIS));
                    continue;
                case '*':
                    tokens.push_back(Token::create(Token::Type::ASTERISK));
                    continue;
                case ',':
                    tokens.push_back(Token::create(Token::Type::COMMA));
                    continue;
                case ';':
                    tokens.push_back(Token::create(Token::Type::SEMICOLON));
                    continue;
                case '<':
                    if (it != std::end(input) && *it == '=') {
                        ++it;
                        tokens.push_back(Token::create(Token::Type::LESS_THAN_OR_EQUAL_TO_SIGN));
                    } else {
                        tokens.push_back(Token::create(Token::Type::LESS_THAN_SIGN));
                    }
                    continue;
                case '=':
                    tokens.push_back(Token::create(Token::Type::EQUAL_TO_SIGN));
                    continue;
                case '>':
                    if (it != std::end(input) && *it == '=') {
                        ++it;
                        tokens.push_back(Token::create(Token::Type::GREATER_THAN_OR_EQUAL_TO_SIGN));
                    } else {
                        tokens.push_back(Token::create(Token::Type::GREATER_THAN_SIGN));
                    }
                    continue;
                default:
                    break;
            }

            --it;

            if (*it == '\'') {
                const auto endCharacter = *it;
                ++it;
                const auto begin = it;
                while (true) {
                    if (it == std::end(input)) {
                        if (errorReporter)
                            errorReporter("Unterminated string literal");
                        return std::nullopt;
                    }

                    if (*it == endCharacter) {
                        tokens.push_back(Token::createString(std::string(begin, it)));
                        ++it;
                        break;
                    }

                    ++it;
                }
                continue;
            }

            if (isDigit(*it)) {
                const char *start = &*it;
                char *end = const_cast<char *>(&*std::end(input));
                auto value = static_cast<std::int64_t>(std::strtoll(start, &end, 10));

                if (start != end) {
                    it += end - start;
                    tokens.push_back(Token::createInt(value));
                    continue;
                }
            }

            if (isIdentifierStart(*it)) {
                std::string identifierName;
                identifierName.reserve(static_cast<std::size_t>(std::distance(it, std::end(input))));
                do {
                    if (it == std::end(input))
                        break;
                    identifierName.push_back(*it++);
                } while (isIdentifierContinue(*it));

                auto reservedIdentifierIterator =
                        std::find_if(std::cbegin(g_reservedIdentifierNames), std::cend(g_reservedIdentifierNames),
                             [&identifierName] (std::string_view reservedIdentifierName) {
                                 return stringEqualsIgnoreCase(reservedIdentifierName, identifierName);
                             }
                        );

                if (reservedIdentifierIterator != std::cend(g_reservedIdentifierNames)) {
                    tokens.push_back(Token::createReservedIdentifier(static_cast<ReservedIdentifier>(
                            std::distance(std::begin(g_reservedIdentifierNames), reservedIdentifierIterator))));
                } else {
                    identifierName.shrink_to_fit();
                    tokens.push_back(Token::createIdentifier(std::move(identifierName)));
                }
                continue;
            }

            if (errorReporter)
                errorReporter(fmt::sprintf("Invalid character in query: 0x%hhx or '%c'\n", *it, *it));
            return std::nullopt;
        }

        return tokens;
    }

} // namespace strawberry
