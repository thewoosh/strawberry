/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string>
#include <variant>
#include <vector>

#include <cstdint>

#include "Source/Data/FixedStringVector.hpp"
#include "Source/Data/Variable.hpp"

namespace strawberry {

#define STRAWBERRY_COLUMN_CONTAINER_VARIANT_TYPES(REGISTER_VARIANT, REGISTER_LAST_VARIANT) \
        REGISTER_VARIANT(std::string) \
        REGISTER_LAST_VARIANT(std::int64_t)

#define CCVARIANT_USING(type) std::vector<type>,
#define CCVARIANT_USING_LAST(type) std::vector<type>
    using ColumnContainerVariant = std::variant<
            FixedStringVector,
            STRAWBERRY_COLUMN_CONTAINER_VARIANT_TYPES(CCVARIANT_USING, CCVARIANT_USING_LAST)
    >;
#undef CCVARIANT_USING
#undef CCVARIANT_USING_LAST

    class ColumnContainer
            : public ColumnContainerVariant {
    public:
        void
        reserve(std::size_t capacity);

        [[nodiscard]] Variable
        operator[](std::size_t index) const;

        [[nodiscard]] std::size_t
        size() const;

    private:
        auto
        doGenericOperator(auto function);
    };

} // namespace strawberry
