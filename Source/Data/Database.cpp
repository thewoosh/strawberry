/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/Data/Database.hpp"

#include <algorithm>

#include "Source/Utility/String.hpp"

namespace strawberry {

    Table *
    Database::tableByName(std::string_view name) {
        auto iterator = std::find_if(std::begin(m_tables), std::end(m_tables), [name] (Table &table) {
            return stringEqualsIgnoreCase(name, table.name());
        });

        return iterator == std::end(m_tables) ? nullptr : iterator.base();
    }

} // namespace strawberry
