/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string>
#include <vector>

namespace strawberry {

    class TableViewPresentation {
    public:
        [[nodiscard]]
        TableViewPresentation() = default;

        [[nodiscard]] inline explicit
        TableViewPresentation(std::vector<std::vector<std::string>> &&columns)
                : m_columns(std::move(columns)) {
        }

        [[nodiscard]] inline std::vector<std::vector<std::string>> &
        columns() {
            return m_columns;
        }

        [[nodiscard]] inline const std::vector<std::vector<std::string>> &
        columns() const {
            return m_columns;
        }

    private:
        std::vector<std::vector<std::string>> m_columns;
    };

} // namespace strawberry
