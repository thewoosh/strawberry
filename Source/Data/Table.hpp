/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string>

#include <cassert>

#include "Source/Data/ColumnContainer.hpp"
#include "Source/Syntax/ColumnSpecifier.hpp"

namespace strawberry {

    class Table {
    public:
        struct ColumnEntry {
            ColumnSpecifier *specifier{};
            ColumnContainer *container{};

            [[nodiscard]] inline constexpr
            explicit operator bool() const {
                return specifier != nullptr && container != nullptr;
            }
        };

        [[nodiscard]] inline
        Table(std::string &&name, std::vector<ColumnSpecifier> &&columnSpecifiers,
              std::vector<ColumnContainer> &&columnContainers)
                : m_name(std::move(name))
                , m_columnSpecifiers(std::move(columnSpecifiers))
                , m_columnContainers(std::move(columnContainers)) {
            assert(std::size(m_columnContainers) == std::size(m_columnSpecifiers));
        }

        [[nodiscard]] inline std::string &
        name() {
            return m_name;
        }

        [[nodiscard]] inline const std::string &
        name() const {
            return m_name;
        }

        [[nodiscard]] inline std::vector<ColumnContainer> &
        columnContainers() {
            return m_columnContainers;
        }

        [[nodiscard]] inline const std::vector<ColumnContainer> &
        columnContainers() const {
            return m_columnContainers;
        }

        [[nodiscard]] inline std::vector<ColumnSpecifier> &
        columnSpecifiers() {
            return m_columnSpecifiers;
        }

        [[nodiscard]] inline const std::vector<ColumnSpecifier> &
        columnSpecifiers() const {
            return m_columnSpecifiers;
        }

        [[nodiscard]] Table::ColumnEntry
        findColumn(std::string_view columnName);

    private:
        std::string m_name;
        std::vector<ColumnSpecifier> m_columnSpecifiers;
        std::vector<ColumnContainer> m_columnContainers{};
    };

} // namespace strawberry
