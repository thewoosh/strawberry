/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <compare>
#include <optional>
#include <variant>
#include <string>

#include <cstdint>

#include "Source/Data/Null.hpp"
#include "Source/Syntax/TypeSpecifier.hpp"

namespace strawberry {

    struct Variable
            : public std::variant<std::string, std::int64_t, bool, NullType> {
        /**
         * The type of the contained value.
         *
         * Note that this is sorted in order of precedence order.
         */
        enum class Type {
            BOOLEAN,
            INTEGER,

            CHARACTERS, // varchar, text

            NULL_TYPE,
        };

        [[nodiscard]] inline explicit
        Variable(NullType)
                : variant(NullType{})
                , m_type(Type::NULL_TYPE) {
        }

        [[nodiscard]] inline explicit
        Variable(bool value)
                : variant(value)
                , m_type(Type::BOOLEAN) {
        }

        [[nodiscard]] inline explicit
        Variable(std::int64_t value)
                : variant(value)
                , m_type(Type::INTEGER) {
        }

        [[nodiscard]] inline explicit
        Variable(std::string &&value)
                : variant(std::move(value))
                , m_type(Type::CHARACTERS) {
        }

        [[nodiscard]] inline explicit
        Variable(std::string_view value)
                : variant(std::string(value))
                , m_type(Type::CHARACTERS) {
        }

        [[nodiscard]] std::weak_ordering
        operator<=>(const Variable &variable) const;

        [[nodiscard]] bool
        operator==(const Variable &variable) const;

        [[nodiscard]] std::int64_t
        asInteger() const;

        [[nodiscard]] std::string
        asString() const;

        [[nodiscard]] bool
        canConvertTo(const TypeSpecifier &typeSpecifier) const;

        [[nodiscard]] std::optional<bool>
        toBoolean() const;

        [[nodiscard]] std::optional<std::int64_t>
        toInteger() const;

        [[nodiscard]] std::string
        toString() const;

        [[nodiscard]] inline constexpr Type
        type() const {
            return m_type;
        }

    private:
        Type m_type;
    };

} // namespace strawberry
