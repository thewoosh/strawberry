/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string_view>
#include <vector>

namespace strawberry {

    struct FixedStringVector {
        [[nodiscard]] inline explicit
        FixedStringVector(std::size_t elementSize)
                : m_elementSize(elementSize) {
        }

        [[nodiscard]] inline std::string_view
        operator[](std::size_t index) const {
            return {&m_data[index * m_elementSize], m_elementSize};
        }

        [[nodiscard]] inline std::string_view
        at(std::size_t index) const {
            return {&m_data.at(index * m_elementSize), m_elementSize};
        }

        inline void
        append(std::string_view sv) {
            std::copy(std::begin(sv), std::end(sv), std::back_inserter(m_data));
        }

        inline void
        reserve(std::size_t count) {
            m_data.reserve(count * m_elementSize);
        }

        [[nodiscard]] inline std::size_t
        size() const {
            return m_data.size() / m_elementSize;
        }

    private:
        std::size_t m_elementSize{};
        std::vector<char> m_data{};
    };

} // namespace strawberry
