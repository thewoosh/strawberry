/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/Data/Variable.hpp"

#include <fmt/format.h>

#include "Source/Utility/Conversions.hpp"

namespace strawberry {

#define DEFINE_COMPARATOR(OP, EQ) \
    switch (std::min(m_type, variable.type())) { \
        case Type::BOOLEAN: \
        case Type::INTEGER: \
            return toInteger() OP variable.toInteger(); \
        case Type::CHARACTERS: \
            return toString() OP variable.toString(); \
        case Type::NULL_TYPE: \
            return EQ; \
    }

    std::weak_ordering
    Variable::operator<=>(const Variable &variable) const {
        DEFINE_COMPARATOR(<=>, std::weak_ordering::equivalent)

        // Some sort of programming error or memory corruption and we cannot
        // recover from that from here.
        std::abort();
    }

    bool
    Variable::operator==(const Variable &variable) const {
        DEFINE_COMPARATOR(==, true)
        return false;
    }

    std::int64_t
    Variable::asInteger() const {
        // TODO type conversion?
        return std::get<std::int64_t>(*this);
    }

    std::string
    Variable::asString() const {
        switch (m_type) {
            case Type::BOOLEAN:
                return std::get<bool>(*this) ? "TRUE" : "FALSE";
            case Type::CHARACTERS:
                return std::get<std::string>(*this);
            case Type::INTEGER:
                return fmt::format_int(std::get<std::int64_t>(*this)).str();
            case Variable::Type::NULL_TYPE:
                return "NULL";
        }

        return "";
    }

    bool
    Variable::canConvertTo(const TypeSpecifier &typeSpecifier) const {
        switch (typeSpecifier.typeName()) {
            case TypeName::INTEGER:
                switch (m_type) {
                    case Type::BOOLEAN:
                        return true;
                    case Type::CHARACTERS:
                        // TODO maybe 1 can be converted to '1' in the future?
                        return false;
                    case Type::INTEGER:
                        return true;
                    case Type::NULL_TYPE:
                        return false;
                }
                return false;
            case TypeName::CHAR:
            case TypeName::VARCHAR:
                // Anything can be converted to a string :)
                return true;
        }

        return false;
    }

    std::optional<bool>
    Variable::toBoolean() const {
        switch (m_type) {
            case Type::BOOLEAN:
                return std::get<bool>(*this);
            case Type::CHARACTERS:
                if (std::get<std::string>(*this).empty())
                    return false;
                return std::nullopt;
            case Type::INTEGER:
                return static_cast<bool>(std::get<std::int64_t>(*this));
            case Variable::Type::NULL_TYPE:
                return false;
        }
        return std::nullopt;
    }

    std::optional<std::int64_t>
    Variable::toInteger() const {
        switch (m_type) {
            case Type::BOOLEAN:
                return static_cast<std::int64_t>(std::get<bool>(*this));
            case Type::CHARACTERS:
                if (auto integer = convertStringToInteger(std::get<std::string>(*this)))
                    return integer;
                return std::nullopt;
            case Type::INTEGER:
                return std::get<std::int64_t>(*this);
            case Variable::Type::NULL_TYPE:
                return std::nullopt;
        }

        return std::nullopt;
    }

    std::string
    Variable::toString() const {
        switch (m_type) {
            case Type::BOOLEAN:
                return std::get<bool>(*this) ? "TRUE" : "FALSE";
            case Type::CHARACTERS:
                return std::get<std::string>(*this);
            case Type::INTEGER:
                return fmt::format_int(std::get<std::int64_t>(*this)).str();
            case Variable::Type::NULL_TYPE:
                return "NULL";
        }

        return fmt::format("<invalid-variable-type-{}>", static_cast<std::size_t>(m_type));
    }

} // namespace strawberry
