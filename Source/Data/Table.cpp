/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/Data/Table.hpp"
#include "Source/Utility/String.hpp"

namespace strawberry {

    Table::ColumnEntry
    Table::findColumn(std::string_view columnName) {
        for (std::size_t index = 0; index < m_columnSpecifiers.size(); ++index) {
            if (stringEqualsIgnoreCase(m_columnSpecifiers[index].name(), columnName)) {
                return Table::ColumnEntry{&m_columnSpecifiers[index], &m_columnContainers[index]};
            }
        }

        return {};
    }

} // namespace strawberry
