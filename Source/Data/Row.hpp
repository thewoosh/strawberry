/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <vector>

#include "Source/Data/Variable.hpp"
#include "Source/Syntax/ColumnSpecifier.hpp"

namespace strawberry {

    class Row {
    public:
        [[nodiscard]] inline
        Row(const std::vector<ColumnSpecifier> &columnSpecifiers, std::vector<Variable> &&columns)
                : m_columnSpecifiers(columnSpecifiers)
                , m_columns(std::move(columns)) {
        }

        [[nodiscard]] inline std::vector<Variable> &
        columns() {
            return m_columns;
        }

        [[nodiscard]] inline const std::vector<Variable> &
        columns() const {
            return m_columns;
        }

        [[nodiscard]] inline const std::vector<ColumnSpecifier> &
        specifiers() const {
            return m_columnSpecifiers;
        }

    private:
        const std::vector<ColumnSpecifier> &m_columnSpecifiers;
        std::vector<Variable> m_columns;
    };

} // namespace strawberry
