/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string>
#include <vector>

#include "Source/Data/Table.hpp"

namespace strawberry {

    class Database {
    public:
        [[nodiscard]] inline explicit
        Database(std::string &&name, std::vector<Table> &&tables={})
                : m_name(std::move(name))
                , m_tables(std::move(tables)) {
        }

        [[nodiscard]] inline std::string &
        name() {
            return m_name;
        }

        [[nodiscard]] inline const std::string &
        name() const {
            return m_name;
        }

        [[nodiscard]] Table *
        tableByName(std::string_view name);

        [[nodiscard]] inline std::vector<Table> &
        tables() {
            return m_tables;
        }

        [[nodiscard]] inline const std::vector<Table> &
        tables() const {
            return m_tables;
        }

    private:
        std::string m_name;
        std::vector<Table> m_tables{};
    };

} // namespace strawberry
