/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/Data/ColumnContainer.hpp"

#include <fmt/format.h>

#include "Source/Utility/Cast.hpp"

namespace strawberry {

    auto
    ColumnContainer::doGenericOperator(auto function) {
        if (auto *vector = std::get_if<FixedStringVector>(downcast<ColumnContainerVariant *>(this))) {
            return function(vector);
        }
#define GENERIC_OPERATOR(type) \
        if (auto *vector = std::get_if<std::vector<type>>(downcast<ColumnContainerVariant *>(this))) { \
            return function(vector); \
        }
        STRAWBERRY_COLUMN_CONTAINER_VARIANT_TYPES(GENERIC_OPERATOR, GENERIC_OPERATOR)
        fmt::print("Error: unable to do generic operation!");
        std::abort();
    }

    Variable
    ColumnContainer::operator[](std::size_t index) const {
        return const_cast<ColumnContainer *>(this)->doGenericOperator([index] (auto *vector) {
            return Variable{vector->at(index)};
        });
    }

    void
    ColumnContainer::reserve(std::size_t capacity) {
        doGenericOperator([capacity] (auto *vector) {
            vector->reserve(capacity);
        });
    }

    std::size_t
    ColumnContainer::size() const {
        std::size_t size{};

        const_cast<ColumnContainer *>(this)->doGenericOperator([&] (auto *vector) {
            size = vector->size();
        });

        return size;
    }

} // namespace strawberry
