/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <vector>

#include "Source/Syntax/ColumnSpecifier.hpp"
#include "Source/Syntax/Query.hpp"

namespace strawberry {

    struct CreateTableQuery
            : public Query {
        [[nodiscard]] inline explicit
        CreateTableQuery(std::string &&name, std::vector<ColumnSpecifier> &&columns = {})
                : Query(Query::Type::CREATE_TABLE)
                , m_tableName(std::move(name))
                , m_columns(std::move(columns)) {
        }

        [[nodiscard]] inline std::string &
        tableName() noexcept {
            return m_tableName;
        }

        [[nodiscard]] inline const std::string &
        tableName() const noexcept {
            return m_tableName;
        }

        [[nodiscard]] inline std::vector<ColumnSpecifier> &
        columns() noexcept {
            return m_columns;
        }

        [[nodiscard]] inline const std::vector<ColumnSpecifier> &
        columns() const noexcept {
            return m_columns;
        }

    private:
        std::string m_tableName;
        std::vector<ColumnSpecifier> m_columns{};
    };

} // namespace strawberry
