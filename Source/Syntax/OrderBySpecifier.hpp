/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include "Source/Syntax/TableColumnQualifier.hpp"

namespace strawberry {

    struct OrderBySpecifier {
        enum class Type {
            ASCENDING,
            DESCENDING
        };

        [[nodiscard]] inline
        OrderBySpecifier(Type type, TableColumnSpecifier &&tableColumnSpecifier)
                : m_type(type)
                , m_tableColumnSpecifier(std::move(tableColumnSpecifier)) {
        }

        [[nodiscard]] inline const TableColumnSpecifier &
        tableColumnSpecifier() const {
            return m_tableColumnSpecifier;
        }

        [[nodiscard]] inline constexpr Type
        type() const {
            return m_type;
        }

    private:
        const Type m_type;
        const TableColumnSpecifier m_tableColumnSpecifier;
    };

} // namespace strawberry
