/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string>

#include "Source/Syntax/Query.hpp"

namespace strawberry {

    struct ShowTablesQuery
            : public Query {
        [[nodiscard]] inline explicit
        ShowTablesQuery(std::string &&databaseName)
                : Query(Query::Type::SHOW_TABLES)
                , m_databaseName(std::move(databaseName)) {
        }

        [[nodiscard]] inline std::string &
        databaseName() noexcept {
            return m_databaseName;
        }

        [[nodiscard]] inline const std::string &
        databaseName() const noexcept {
            return m_databaseName;
        }

    private:
        std::string m_databaseName;
    };

} // namespace strawberry
