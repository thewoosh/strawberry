/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string>

#include "Source/Syntax/Query.hpp"

namespace strawberry {

    struct DescribeQuery
            : public Query {
        [[nodiscard]] inline explicit
        DescribeQuery(std::string &&name)
                : Query(Query::Type::DESCRIBE)
                , m_tableName(std::move(name)) {
        }

        [[nodiscard]] inline std::string &
        tableName() noexcept {
            return m_tableName;
        }

        [[nodiscard]] inline const std::string &
        tableName() const noexcept {
            return m_tableName;
        }

    private:
        std::string m_tableName;
    };

} // namespace strawberry
