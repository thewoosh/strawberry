/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <memory>
#include <string>
#include <vector>

#include "Source/Expression/Expression.hpp"
#include "Source/Syntax/Aliasable.hpp"
#include "Source/Syntax/OrderBySpecifier.hpp"
#include "Source/Syntax/TableColumnQualifier.hpp"

namespace strawberry {

    struct SelectFromQuery
            : public Query {
        [[nodiscard]] inline explicit
        SelectFromQuery(std::vector<Aliasable<TableColumnSpecifier>> &&columns = {},
                        std::vector<Aliasable<std::string>> &&tables = {},
                        std::unique_ptr<Expression> &&whereExpression = {},
                        std::vector<OrderBySpecifier> &&orderBySpecifiers = {})
                : Query(Query::Type::SELECT_FROM)
                , m_columns(std::move(columns))
                , m_tables(std::move(tables))
                , m_whereExpression(std::move(whereExpression))
                , m_orderBySpecifiers(std::move(orderBySpecifiers)) {
        }

        [[nodiscard]] inline std::vector<Aliasable<TableColumnSpecifier>> &
        columns() {
            return m_columns;
        }

        [[nodiscard]] inline const std::vector<Aliasable<TableColumnSpecifier>> &
        columns() const {
            return m_columns;
        }

        [[nodiscard]] inline std::vector<OrderBySpecifier> &
        orderBySpecifiers() {
            return m_orderBySpecifiers;
        }

        [[nodiscard]] inline const std::vector<OrderBySpecifier> &
        orderBySpecifiers() const {
            return m_orderBySpecifiers;
        }

        [[nodiscard]] inline std::vector<Aliasable<std::string>> &
        tables() {
            return m_tables;
        }

        [[nodiscard]] inline const std::vector<Aliasable<std::string>> &
        tables() const noexcept {
            return m_tables;
        }

        [[nodiscard]] inline std::unique_ptr<Expression> &
        whereExpression() {
            return m_whereExpression;
        }

        [[nodiscard]] inline const std::unique_ptr<Expression> &
        whereExpression() const {
            return m_whereExpression;
        }

    private:
        std::vector<Aliasable<TableColumnSpecifier>> m_columns;
        std::vector<Aliasable<std::string>> m_tables{};
        std::unique_ptr<Expression> m_whereExpression;
        std::vector<OrderBySpecifier> m_orderBySpecifiers;
    };

} // namespace strawberry
