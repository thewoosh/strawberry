/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string>

#include "Source/Syntax/TypeSpecifier.hpp"

namespace strawberry {

    struct ColumnSpecifier {
        [[nodiscard]] inline
        ColumnSpecifier(std::string &&name, TypeSpecifier typeSpecifier)
                : m_name(std::move(name))
                , m_typeSpecifier(typeSpecifier) {
        }

        [[nodiscard]] inline std::string &
        name() noexcept {
            return m_name;
        }

        [[nodiscard]] inline const std::string &
        name() const noexcept {
            return m_name;
        }

        [[nodiscard]] inline constexpr TypeSpecifier
        typeSpecifier() const {
            return m_typeSpecifier;
        }

    private:
        std::string m_name;
        const TypeSpecifier m_typeSpecifier;
    };

} // namespace strawberry
