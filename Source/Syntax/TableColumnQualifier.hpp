/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string>

namespace strawberry {

    // The Table-Column Specifier.
    // This class represents the possible column-from-table specifiers.
    //
    // For example:
    // TABLE     COLUMN    COMMENT
    // ""        "*"       all columns from all tables
    // "*"       "*"       all columns from all tables
    // ""        "id"      the unique column id from any of the tables
    // "client"  "id"      the column "id" from the "client" table
    struct TableColumnSpecifier {
        [[nodiscard]] inline
        TableColumnSpecifier(std::string &&tableName, std::string &&columnName)
                : m_tableName(std::move(tableName))
                , m_columnName(std::move(columnName)) {
        }

        [[nodiscard]] std::string
        asFormattedString() const;

        [[nodiscard]] inline const std::string &
        columnName() const {
            return m_columnName;
        }

        [[nodiscard]] inline const std::string &
        tableName() const {
            return m_tableName;
        }

    private:
        std::string m_tableName;
        std::string m_columnName;
    };

} // namespace strawberry
