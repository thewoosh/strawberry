/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <array>
#include <optional>
#include <string_view>

#include "ReservedIdentifier.hpp"

namespace strawberry {

    // REGISTER_TYPE_NAME(identifier name, bool takesLength)
    #define STRAWBERRY_LIST_TYPE_NAMES(REGISTER_TYPE_NAME) \
            REGISTER_TYPE_NAME(CHAR, true) \
            REGISTER_TYPE_NAME(INTEGER, false) \
            REGISTER_TYPE_NAME(VARCHAR, true) \

    enum class TypeName {
        #define STRAWBERRY_TYPE_NAME_ENUMERATOR(name, _) name,
        STRAWBERRY_LIST_TYPE_NAMES(STRAWBERRY_TYPE_NAME_ENUMERATOR)
        #undef STRAWBERRY_TYPE_NAME_ENUMERATOR
    };

    constexpr const std::array g_typeNameStrings{
        #define STRAWBERRY_TYPE_NAME_ENUMERATOR(name, _) std::string_view{#name},
        STRAWBERRY_LIST_TYPE_NAMES(STRAWBERRY_TYPE_NAME_ENUMERATOR)
        #undef STRAWBERRY_TYPE_NAME_ENUMERATOR
    };

    constexpr const std::string_view g_typeNameCombinedAsString {
        #define STRAWBERRY_TYPE_NAME_ENUMERATOR(name, _) #name", "
        STRAWBERRY_LIST_TYPE_NAMES(STRAWBERRY_TYPE_NAME_ENUMERATOR)
        #undef STRAWBERRY_TYPE_NAME_ENUMERATOR
    };

    [[nodiscard]] inline constexpr std::optional<TypeName>
    convertReservedIdentifierToTypeName(ReservedIdentifier reservedIdentifier) {
        switch (reservedIdentifier) {
            #define STRAWBERRY_TYPE_NAME_ENUMERATOR(name, _) case ReservedIdentifier::name: return TypeName::name;
            STRAWBERRY_LIST_TYPE_NAMES(STRAWBERRY_TYPE_NAME_ENUMERATOR)
            #undef STRAWBERRY_TYPE_NAME_ENUMERATOR

            // Non-types
            case ReservedIdentifier::AS:
            case ReservedIdentifier::ASC:
            case ReservedIdentifier::BY:
            case ReservedIdentifier::CREATE:
            case ReservedIdentifier::DATABASES:
            case ReservedIdentifier::DESC:
            case ReservedIdentifier::DESCRIBE:
            case ReservedIdentifier::FROM:
            case ReservedIdentifier::INSERT:
            case ReservedIdentifier::IN:
            case ReservedIdentifier::INTO:
            case ReservedIdentifier::LIKE:
            case ReservedIdentifier::ORDER:
            case ReservedIdentifier::SCHEMAS:
            case ReservedIdentifier::SELECT:
            case ReservedIdentifier::SHOW:
            case ReservedIdentifier::TABLE:
            case ReservedIdentifier::TABLES:
            case ReservedIdentifier::VALUES:
            case ReservedIdentifier::WHERE:
                return std::nullopt;
        }

        return std::nullopt;
    }

    [[nodiscard]] inline constexpr std::string_view
    toString(TypeName typeName) {
        const auto index = static_cast<std::size_t>(typeName);
        if (index >= std::size(g_typeNameStrings))
            return "(invalid)";
        return g_typeNameStrings[index];
    }

    [[nodiscard]] inline constexpr bool
    doesTypeNameTakeLength(TypeName typeName) {
        switch (typeName) {
            #define STRAWBERRY_TYPE_NAME_ENUMERATOR(name, takesLength) case TypeName::name: return takesLength;
            STRAWBERRY_LIST_TYPE_NAMES(STRAWBERRY_TYPE_NAME_ENUMERATOR)
            #undef STRAWBERRY_TYPE_NAME_ENUMERATOR
        }

        return false;
    }

} // namespace strawberry
