/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <vector>

#include "Source/Data/Variable.hpp"
#include "Source/Syntax/ColumnSpecifier.hpp"
#include "Source/Syntax/Query.hpp"

namespace strawberry {

    struct InsertIntoQuery
            : public Query {
        [[nodiscard]] inline explicit
        InsertIntoQuery(std::string &&tableName, std::vector<std::string> &&columnNames = {},
                         std::vector<std::vector<Variable>> &&values = {})
                : Query(Query::Type::INSERT_INTO)
                , m_tableName(std::move(tableName))
                , m_columnNames(std::move(columnNames))
                , m_values(std::move(values)) {
        }

        [[nodiscard]] inline std::string &
        tableName() noexcept {
            return m_tableName;
        }

        [[nodiscard]] inline const std::string &
        tableName() const noexcept {
            return m_tableName;
        }

        [[nodiscard]] inline std::vector<std::string> &
        columnNames() noexcept {
            return m_columnNames;
        }

        [[nodiscard]] inline const std::vector<std::string> &
        columnNames() const noexcept {
            return m_columnNames;
        }

        [[nodiscard]] inline std::vector<std::vector<Variable>> &
        values() noexcept {
            return m_values;
        }

        [[nodiscard]] inline const std::vector<std::vector<Variable>> &
        values() const noexcept {
            return m_values;
        }

    private:
        std::string m_tableName;
        std::vector<std::string> m_columnNames{};
        std::vector<std::vector<Variable>> m_values{};
    };

} // namespace strawberry
