/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/Syntax/TableColumnQualifier.hpp"

#include <fmt/format.h>

namespace strawberry {

    std::string
    TableColumnSpecifier::asFormattedString() const {
        if (std::empty(m_tableName))
            return fmt::format("`{}`", m_columnName);
        return fmt::format("`{}`.`{}`", m_tableName, m_columnName);
    }

} // namespace strawberry
