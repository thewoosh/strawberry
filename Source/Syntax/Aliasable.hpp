/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string>

namespace strawberry {

    /**
     * The container for a type that may be an alias. When it *is* an alias, the
     * original acts like the value the alias is pointing to.
     */
    template <typename OriginalType, typename AliasNameType = std::string>
    struct Aliasable {
        [[nodiscard]] inline explicit
        Aliasable(OriginalType &&original, AliasNameType &&aliasName={})
                : m_original(std::move(original))
                , m_aliasName(std::move(aliasName)) {
        }

        [[nodiscard]] inline AliasNameType &
        aliasName() {
            return m_aliasName;
        }

        [[nodiscard]] inline const AliasNameType &
        aliasName() const {
            return m_aliasName;
        }

        [[nodiscard]] inline bool
        hasAlias() const {
            return !std::empty(m_aliasName);
        }

        [[nodiscard]] inline OriginalType &
        original() {
            return m_original;
        }

        [[nodiscard]] inline const OriginalType &
        original() const {
            return m_original;
        }

    private:
        OriginalType m_original{};
        AliasNameType m_aliasName;
    };

} // namespace strawberry
