/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include "Source/Syntax/TypeName.hpp"

#include <string>

#include <cstddef> // for std::size_t

namespace strawberry {

    struct TypeSpecifier {
        [[nodiscard]] inline constexpr explicit
        TypeSpecifier(TypeName typeName, std::size_t maxSize = 0)
                : m_typeName(typeName)
                , m_maxSize(maxSize) {
        }

        [[nodiscard]] inline constexpr std::size_t
        maxSize() const {
            return m_maxSize;
        }

        [[nodiscard]] std::string
        toString() const;

        [[nodiscard]] inline constexpr TypeName
        typeName() const {
            return m_typeName;
        }

    private:
        const TypeName m_typeName;
        std::size_t m_maxSize{0};
    };

} // namespace strawberry
