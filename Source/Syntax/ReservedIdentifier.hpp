/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <array>
#include <string_view>

namespace strawberry {

    #define STRAWBERRY_LIST_RESERVED_IDENTIFIERS(REGISTER_RESERVED_IDENTIFIER) \
            REGISTER_RESERVED_IDENTIFIER(AS) \
            REGISTER_RESERVED_IDENTIFIER(ASC) \
            REGISTER_RESERVED_IDENTIFIER(BY) \
            REGISTER_RESERVED_IDENTIFIER(CHAR) \
            REGISTER_RESERVED_IDENTIFIER(CREATE) \
            REGISTER_RESERVED_IDENTIFIER(DATABASES) \
            REGISTER_RESERVED_IDENTIFIER(DESC) \
            REGISTER_RESERVED_IDENTIFIER(DESCRIBE) \
            REGISTER_RESERVED_IDENTIFIER(FROM) \
            REGISTER_RESERVED_IDENTIFIER(INSERT) \
            REGISTER_RESERVED_IDENTIFIER(INTEGER) \
            REGISTER_RESERVED_IDENTIFIER(IN) \
            REGISTER_RESERVED_IDENTIFIER(INTO) \
            REGISTER_RESERVED_IDENTIFIER(LIKE) \
            REGISTER_RESERVED_IDENTIFIER(ORDER) \
            REGISTER_RESERVED_IDENTIFIER(SCHEMAS) \
            REGISTER_RESERVED_IDENTIFIER(SELECT) \
            REGISTER_RESERVED_IDENTIFIER(SHOW) \
            REGISTER_RESERVED_IDENTIFIER(TABLE) \
            REGISTER_RESERVED_IDENTIFIER(TABLES) \
            REGISTER_RESERVED_IDENTIFIER(VALUES) \
            REGISTER_RESERVED_IDENTIFIER(VARCHAR) \
            REGISTER_RESERVED_IDENTIFIER(WHERE) \

    enum class ReservedIdentifier {
        #define STRAWBERRY_RESERVED_IDENTIFIER_ENUMERATOR(name) name,
        STRAWBERRY_LIST_RESERVED_IDENTIFIERS(STRAWBERRY_RESERVED_IDENTIFIER_ENUMERATOR)
        #undef STRAWBERRY_RESERVED_IDENTIFIER_ENUMERATOR
    };

    constexpr const std::array g_reservedIdentifierNames{
        #define STRAWBERRY_RESERVED_IDENTIFIER_ENUMERATOR(name) std::string_view{#name},
        STRAWBERRY_LIST_RESERVED_IDENTIFIERS(STRAWBERRY_RESERVED_IDENTIFIER_ENUMERATOR)
        #undef STRAWBERRY_RESERVED_IDENTIFIER_ENUMERATOR
    };

    [[nodiscard]] inline constexpr std::string_view
    toString(ReservedIdentifier reservedIdentifier) {
        const auto index = static_cast<std::size_t>(reservedIdentifier);
        if (index >= std::size(g_reservedIdentifierNames))
            return "(invalid)";
        return g_reservedIdentifierNames[index];
    }

} // namespace strawberry
