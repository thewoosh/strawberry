/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/Syntax/TypeSpecifier.hpp"

#include <fmt/format.h>

namespace strawberry {

    std::string
    TypeSpecifier::toString() const {
        switch (m_typeName) {
            case TypeName::CHAR:
                return fmt::format("char({})", m_maxSize);
            case TypeName::INTEGER:
                return "integer";
            case TypeName::VARCHAR:
                return fmt::format("varchar({})", m_maxSize);
        }

        return fmt::format("<invalid-typename-{}>", static_cast<std::size_t>(m_typeName));
    }

} // namespace strawberry
