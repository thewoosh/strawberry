/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

namespace strawberry {

    /**
     * The generic underlying type of all queries.
     */
    struct Query {
        enum class Type {
            CREATE_TABLE,
            DESCRIBE,
            INSERT_INTO,
            SELECT_FROM,
            SHOW_DATABASES,
            SHOW_TABLES,
        };

        [[nodiscard]] inline constexpr explicit
        Query(Type type)
                : m_type(type) {
        }

        virtual
        ~Query() = default;

        [[nodiscard]] inline constexpr Type
        type() const {
            return m_type;
        }

    private:
        const Type m_type;
    };

} // namespace strawberry
