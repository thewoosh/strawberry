/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/StaticAnalysis/StaticAnalyzer.hpp"

#include <fmt/format.h>

#include "Source/Expression/Expression.hpp"
#include "Source/Strawberry.hpp"
#include "Source/Syntax/Query.hpp"
#include "Source/Syntax/SelectFromQuery.hpp"

namespace strawberry {

    StaticAnalysisResult
    StaticAnalyzer::analyzeQuerySelectFrom(const SelectFromQuery *query) {
        if (m_instance->currentDatabase() == nullptr) {
            postQueryError("Cannot do a SELECT on a table when no database is selected");
            return StaticAnalysisResult::ERRONEOUS;
        }

        if (m_instance->currentDatabase()->tables().empty()) {
            postQueryError(fmt::format("No tables in database \"{}\"", m_instance->currentDatabase()->name()));
            return StaticAnalysisResult::ERRONEOUS;
        }

        for (const auto &table : query->tables()) {
            if (m_instance->currentDatabase()->tableByName(table.original()) == nullptr) {
                if (table.hasAlias()) {
                    postQueryError(fmt::format(R"(Unknown table "{}" aliased "{}", did you mean "{}"?)",
                            table.original(), table.aliasName(), getClosestTableName(table.original())));
                } else {
                    postQueryError(fmt::format(R"(Unknown table "{}", did you mean "{}"?)", table.original(),
                            getClosestTableName(table.original())));
                }
                return StaticAnalysisResult::ERRONEOUS;
            }
        }

        if (query->whereExpression())
            return analyze(query->whereExpression().get());
        return StaticAnalysisResult::OK;
    }

} // namespace strawberry
