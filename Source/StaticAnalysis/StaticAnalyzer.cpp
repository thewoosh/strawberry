/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/StaticAnalysis/StaticAnalyzer.hpp"

#include "Source/Strawberry.hpp"
#include "Source/Syntax/Query.hpp"
#include "Source/Syntax/SelectFromQuery.hpp"
#include "Source/Utility/Cast.hpp"
#include "Source/Utility/StringList.hpp"

namespace strawberry {

    StaticAnalysisResult
    StaticAnalyzer::analyze(const Expression *expression) {
        static_cast<void>(expression);
        return StaticAnalysisResult::OK;
    }

    StaticAnalysisResult
    StaticAnalyzer::analyze(const Query *query) {
        if (query->type() == Query::Type::SELECT_FROM) {
            return analyzeQuerySelectFrom(downcast<const SelectFromQuery *>(query));
        }

        return StaticAnalysisResult::OK;
    }

    std::string_view
    StaticAnalyzer::getClosestTableName(std::string_view name) {
        if (m_instance == nullptr || m_instance->currentDatabase() == nullptr)
            return {};

        std::vector<std::string_view> names;
        names.reserve(std::size(m_instance->currentDatabase()->tables()));

        const auto &tables = m_instance->currentDatabase()->tables();
        std::transform(std::cbegin(tables), std::cend(tables), std::back_inserter(names),
                       [] (const auto &table) -> std::string_view { return table.name(); });

        auto *value = getClosestMatchingString(names, name);
        return value ? *value : std::string_view{};
    }

    void
    StaticAnalyzer::postQueryError(std::string_view message) const {
        if (m_messageInterface.onQueryError)
            m_messageInterface.onQueryError(message);
    }

} // namespace strawberry
