/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include "Source/StaticAnalysis/Result.hpp"

namespace strawberry {

    struct Expression;
    struct MessageInterface;
    struct Query;
    struct SelectFromQuery;
    class Strawberry;

    class StaticAnalyzer {
    public:
        [[nodiscard]] inline
        StaticAnalyzer(const Strawberry *instance, MessageInterface &messageInterface)
                : m_instance(instance)
                , m_messageInterface(messageInterface) {
        }

        [[nodiscard]] StaticAnalysisResult
        analyze(const Query *query);

    private:
        const Strawberry *const m_instance;
        MessageInterface &m_messageInterface;

        [[nodiscard]] StaticAnalysisResult
        analyze(const Expression *expression);

        [[nodiscard]] StaticAnalysisResult
        analyzeQuerySelectFrom(const SelectFromQuery *query);

        [[nodiscard]] std::string_view
        getClosestTableName(std::string_view name);

        void
        postQueryError(std::string_view) const;
    };

} // namespace strawberry
