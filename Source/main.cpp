/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include <iostream>
#include <string>

#include <csignal>

#include <fmt/format.h>

#include "Source/Strawberry.hpp"
#include "Source/Utility/String.hpp"
#include "Source/Utility/Types.hpp"

enum class ExitCode {
    SUCCESS,
};

void
signalInterruptHandler([[maybe_unused]] int signum) {
    fmt::print("\nGoodbye!\n");
    std::exit(strawberry::toUnderlying(ExitCode::SUCCESS));
}

void
printStartupInformation() {
    fmt::print("Strawberry SQL version {}.{}", STRAWBERRY_VERSION_MAJOR, STRAWBERRY_VERSION_MINOR);
#if (STRAWBERRY_VERSION_PATCH != 0)
    fmt::print("{}\n", STRAWBERRY_VERSION_PATCH);
#else
    fmt::print("\n");
#endif
}

int main() {
    signal(SIGINT, signalInterruptHandler);
    signal(SIGTERM, signalInterruptHandler);

    printStartupInformation();

    strawberry::Strawberry strawberryInstance{strawberry::MessageInterface::createStdout()};

    auto showTableViewPresentation = [] (const strawberry::TableViewPresentation &tableViewPresentation) {
        const auto columnCount = std::size(tableViewPresentation.columns());
        const auto rowCount = std::size(tableViewPresentation.columns().front());

        std::vector<std::size_t> widths;
        widths.reserve(columnCount);

        // calculate width per column
        for (const auto &column : tableViewPresentation.columns()) {
            std::size_t max = 0;
            for (const auto &row : column) {
                max = std::max(max, row.length());
            }

            if (rowCount != column.size()) {
                fmt::print("Error: row sizes inconsistent!\n");
                return;
            }

            widths.push_back(max + 2);
        }

        auto printSeparationLine = [&] () {
            for (const auto &width : widths) {
                std::putchar('+');
                std::fputs(std::string(width, '-').c_str(), stdout);
            }
            std::fputs("+\n", stdout);
        };

        printSeparationLine();
        for (std::size_t row = 0; row < rowCount; ++row) {
            fmt::print("|");
            for (std::size_t column = 0; column < columnCount; ++column) {
                const auto &string = tableViewPresentation.columns()[column][row];
                const auto leftOverWidth = widths[column] - string.length();
                fmt::print(" {}{}|", string, std::string(leftOverWidth - 1, ' '));
            }

            std::putchar('\n');
            printSeparationLine();
        }
    };

    while (true) {
        fmt::print("> ");
        std::string input;
        std::getline(std::cin, input);

        auto command = strawberry::trim(std::string_view{input});
        if (command == "q" || command == "quit" || command == "exit")
            break;

        auto tableViewPresentation = strawberryInstance.processQuery(command);
        if (!tableViewPresentation.columns().empty())
            showTableViewPresentation(tableViewPresentation);
    }

    return strawberry::toUnderlying(ExitCode::SUCCESS);
}
