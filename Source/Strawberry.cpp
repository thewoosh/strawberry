/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/Strawberry.hpp"

#include <algorithm>
#include <map>
#include <string>
#include <vector>
#include <utility>

#include <cassert>

#include <fmt/format.h>

#include "Source/Data/FixedStringVector.hpp"
#include "Source/Expression/BiExpression.hpp"
#include "Source/Expression/LikeExpression.hpp"
#include "Source/Expression/PrimaryExpression.hpp"
#include "Source/StaticAnalysis/StaticAnalyzer.hpp"
#include "Source/Syntax/CreateTableQuery.hpp"
#include "Source/Syntax/DescribeQuery.hpp"
#include "Source/Syntax/InsertIntoQuery.hpp"
#include "Source/Syntax/SelectFromQuery.hpp"
#include "Source/Syntax/ShowTablesQuery.hpp"
#include "Source/Utility/Cast.hpp"
#include "Source/Utility/String.hpp"
#include "Source/Utility/StringList.hpp"

namespace strawberry {

    inline constexpr std::size_t kLikeRecursionLimit = 32;

    Strawberry::Strawberry(MessageInterface messageInterface)
            : m_messageInterface(std::move(messageInterface)) {
        m_currentDatabase = &m_databases.emplace_back("main");
    }

    Database *
    Strawberry::databaseByName(std::string_view name) {
        auto iterator = std::find_if(std::begin(m_databases), std::end(m_databases), [name] (Database &database) {
            return stringEqualsIgnoreCase(name, database.name());
        });

        return iterator == std::end(m_databases) ? nullptr : iterator.base();
    }

    std::string_view
    Strawberry::getClosestDatabaseName(std::string_view name) const {
        if (std::empty(m_databases))
            return {};

        std::vector<std::string_view> names;
        names.reserve(std::size(m_databases));
        std::transform(std::cbegin(m_databases), std::cend(m_databases), std::back_inserter(names),
                       [] (const auto &database) -> std::string_view { return database.name(); });

        auto *value = getClosestMatchingString(names, name);
        return value ? *value : std::string_view{};
    }

    [[nodiscard]] std::optional<bool>
    Strawberry::evaluateComparisonExpression(const BiExpression *expression, const VariableList &variableList) const {
        auto lhs = evaluateExpression(expression->lhs().get(), variableList);
        if (!lhs) {
            postQueryError("failed to evaluate left hand side of bi-expression");
            return std::nullopt;
        }

        auto rhs = evaluateExpression(expression->rhs().get(), variableList);
        if (!rhs) {
            postQueryError("failed to evaluate right hand side of bi-expression");
            return std::nullopt;
        }

        switch (expression->type()) {
            case Expression::Type::COMPARE_EQUAL_TO:
                return lhs.value() == rhs.value();
            case Expression::Type::COMPARE_GREATER_THAN:
                return lhs.value() > rhs.value();
            case Expression::Type::COMPARE_GREATER_THAN_OR_EQUAL:
                return lhs.value() >= rhs.value();
            case Expression::Type::COMPARE_LESS_THAN:
                return lhs.value() < rhs.value();
            case Expression::Type::COMPARE_LESS_THAN_OR_EQUAL:
                return lhs.value() <= rhs.value();
            case Expression::Type::COMPARE_NOT_EQUAL_TO:
                return lhs.value() != rhs.value();
            default:
                // Illegal!
                return false;
        }
    }

    std::optional<Variable>
    Strawberry::evaluateExpression(const Expression *expression, const VariableList &variableList) const {
        if (!expression)
            return std::nullopt;

        switch (expression->type()) {
            case Expression::Type::PRIMARY_IDENTIFIER: {
                const auto &identifier = downcast<const PrimaryExpression<std::string> *>(expression)->data();
                auto it = std::find_if(std::cbegin(variableList), std::cend(variableList), [&](const auto &entry) {
                    return stringEqualsIgnoreCase(entry.first, identifier);
                });
                if (it == std::cend(variableList)) {
                    std::vector<std::string_view> names{};
                    names.reserve(std::size(variableList));
                    std::transform(std::cbegin(variableList), std::cend(variableList), std::back_inserter(names),
                                   [] (const auto &entry) -> std::string_view { return entry.first; });
                    if (auto closest = getClosestMatchingString(names, identifier); closest && !closest->empty())
                        postQueryError(fmt::format(R"(invalid identifier "{}", did you mean "{}")", identifier, *closest));
                    else
                        postQueryError(fmt::format(R"(invalid identifier "{}")", identifier));
                    return std::nullopt;
                }
                return it->second;
            }
            case Expression::Type::PRIMARY_INTEGER:
                return Variable{downcast<const PrimaryExpression<std::int64_t> *>(expression)->data()};
            case Expression::Type::PRIMARY_STRING:
                return Variable{downcast<const PrimaryExpression<std::string> *>(expression)->data()};
            case Expression::Type::COMPARE_EQUAL_TO:
            case Expression::Type::COMPARE_GREATER_THAN:
            case Expression::Type::COMPARE_GREATER_THAN_OR_EQUAL:
            case Expression::Type::COMPARE_LESS_THAN:
            case Expression::Type::COMPARE_LESS_THAN_OR_EQUAL:
            case Expression::Type::COMPARE_NOT_EQUAL_TO: {
                auto result = evaluateComparisonExpression(downcast<const BiExpression *>(expression), variableList);
                if (!result.has_value())
                    return std::nullopt;
                return Variable{result.value()};
            }
            case Expression::Type::LIKE:
                if (auto result = evaluateLikeExpression(downcast<const LikeExpression *>(expression), variableList))
                    return Variable{result.value()};
                return std::nullopt;
        }

        postQueryError(fmt::format("unknown query type {}", expression->type()));
        return std::nullopt;
    }

    std::optional<bool>
    Strawberry::evaluateLikeExpression(const LikeExpression *expression, const VariableList &variableList) const {
        const auto iter = std::find_if(std::cbegin(variableList), std::cend(variableList), [&] (const auto &entry) {
            return stringEqualsIgnoreCase(entry.first, expression->tableColumnSpecifier().columnName());
        });

        if (iter == std::cend(variableList)) {
            postQueryError(fmt::format("unknown column/variable \"{}\"", expression->tableColumnSpecifier().columnName()));
            return std::nullopt;
        }

        const Variable &variable = iter->second;
        const auto stringToMatch = variable.toString();
        if (std::empty(stringToMatch)) {
            return std::empty(expression->matchString()) || expression->matchString() == "%";
        }

        if (std::empty(expression->matchString())) {
            return true;
        }

        using IterType = decltype(std::cbegin(stringToMatch));
        static_assert(std::is_same_v<IterType, decltype(std::cbegin(expression->matchString()))>);

        std::size_t recursionCounter = 1;

        std::function<bool (IterType, IterType)> matcher =
                [&] (IterType stringIter, IterType matchIter) {
            while (true) {
                if (stringIter == std::cend(stringToMatch) && matchIter == std::cend(expression->matchString()))
                    return true;
                if (stringIter == std::cend(stringToMatch) || matchIter == std::cend(expression->matchString()))
                    return false;
                while (*matchIter == '%') {
                    if (std::next(matchIter) == std::cend(expression->matchString()))
                        return true;
                    while (true) {
                        if (stringIter == std::cend(stringToMatch))
                            return false;
                        if (*stringIter == *std::next(matchIter)) {
                            if (recursionCounter != kLikeRecursionLimit)
                                if (matcher(stringIter, std::next(matchIter)))
                                    return true;
                        }
                        ++stringIter;
                    }
                }
                if (*matchIter != *stringIter)
                    return false;
                ++matchIter;
                ++stringIter;
            }
        };

        return matcher(std::cbegin(stringToMatch), std::cbegin(expression->matchString()));
    }

    Strawberry::SubroutineResult
    Strawberry::invokeWhereClauseOnRow(const Row &row, const Row &byProductRow, const Expression *expression) const {
        if (std::empty(row.columns())) {
            postQueryError("InternalError: row-empty-in-where");
            return Strawberry::SubroutineResult::INTERNAL_ERROR;
        }

        if (std::size(row.columns()) != std::size(row.specifiers())) {
            postQueryError("InternalError: column-count-doesn't-match-specifier-count");
            return Strawberry::SubroutineResult::INTERNAL_ERROR;
        }

        if (std::size(byProductRow.columns()) != std::size(byProductRow.specifiers())) {
            postQueryError("InternalError: by-product-column-count-doesn't-match-specifier-count");
            return Strawberry::SubroutineResult::INTERNAL_ERROR;
        }

        VariableList variables{};
        for (std::size_t i = 0; i < std::size(row.columns()); ++i) {
            variables.emplace(std::string_view{row.specifiers()[i].name()}, row.columns()[i]);
        }

        for (std::size_t i = 0; i < std::size(byProductRow.columns()); ++i) {
            variables.emplace(std::string_view{byProductRow.specifiers()[i].name()}, byProductRow.columns()[i]);
        }

        const auto possibleBoolean = evaluateExpression(expression, variables)->toBoolean();
        if (!possibleBoolean.has_value()) {
            postQueryError("expression resulted in type {} and cannot be converted to a BOOLEAN");
            return Strawberry::SubroutineResult::ERROR;
        }

        return possibleBoolean.value() ? Strawberry::SubroutineResult::TRUE : Strawberry::SubroutineResult::FALSE;
    }

    TableViewPresentation
    Strawberry::processCreateTableQuery(CreateTableQuery *query) {
        if (m_currentDatabase == nullptr) {
            postQueryError(fmt::format("cannot create table when no database is selected."));
            return {};
        }

        if (m_currentDatabase->tableByName(query->tableName()) != nullptr) {
            postQueryError(fmt::format(R"(a table named "{}" already exists in database "{}")", query->tableName(),
                       m_currentDatabase->name()));
            return {};
        }

        std::vector<ColumnContainer> columnContainers{};
        columnContainers.reserve(std::size(query->columns()));

        for (const auto &columnSpecifier : query->columns()) {
            switch (columnSpecifier.typeSpecifier().typeName()) {
                case TypeName::INTEGER:
                    columnContainers.push_back(ColumnContainer{std::vector<std::int64_t>()});
                    break;
                case TypeName::CHAR:
                    columnContainers.push_back(ColumnContainer{FixedStringVector(columnSpecifier.typeSpecifier().maxSize())});
                    break;
                case TypeName::VARCHAR:
                    columnContainers.push_back(ColumnContainer{std::vector<std::string>()});
                    break;
            }
        }

        auto &table = m_currentDatabase->tables().emplace_back(std::move(query->tableName()),
                                                               std::move(query->columns()),
                                                               std::move(columnContainers));

        postInformationalMessage(fmt::format(R"(OK! Created table "{}" in database "{}")", table.name(),
                                 m_currentDatabase->name()));
        return {};
    }

    TableViewPresentation
    Strawberry::processDescribeQuery(DescribeQuery *query) {
        if (m_currentDatabase == nullptr) {
            postQueryError(fmt::format("cannot describe a table when no database is selected."));
            return {};
        }

        const Table *const table = m_currentDatabase->tableByName(query->tableName());
        if (table == nullptr) {
            postQueryError(fmt::format("table \"{}\" does not exist.", query->tableName()));
            return {};
        }

        TableViewPresentation tableViewPresentation{};
        tableViewPresentation.columns().resize(3);
        for (std::size_t i = 0; i < 3; ++i)
            tableViewPresentation.columns()[i].reserve(1 + std::size(table->columnSpecifiers()));

        tableViewPresentation.columns()[0].emplace_back("Name");
        tableViewPresentation.columns()[1].emplace_back("Type");
        tableViewPresentation.columns()[2].emplace_back("Max Size");

        for (const auto &columnSpecifier : table->columnSpecifiers()) {
            tableViewPresentation.columns()[0].push_back(columnSpecifier.name());
            tableViewPresentation.columns()[1].emplace_back(toString(columnSpecifier.typeSpecifier().typeName()));
            tableViewPresentation.columns()[2].push_back(fmt::format_int(columnSpecifier.typeSpecifier().maxSize()).str());
        }

        return tableViewPresentation;
    }

    TableViewPresentation
    Strawberry::processInsertIntoQuery(InsertIntoQuery *query) {
        if (m_currentDatabase == nullptr) {
            postQueryError(fmt::format("cannot insert into a table when no database is selected."));
            return {};
        }

        Table *const table = m_currentDatabase->tableByName(query->tableName());
        if (table == nullptr) {
            postQueryError(fmt::format("table \"{}\" does not exist.", query->tableName()));
            return {};
        }

        assert(query->columnNames().empty());

        // validation step. we don't want to insert half the query if the other half fails.
        for (std::size_t rowIndex = 0; rowIndex < query->values().size(); ++rowIndex) {
            const auto &row = query->values()[rowIndex];

            // Too many columns in to-be-inserted row
            if (row.size() != table->columnSpecifiers().size()) {
                postQueryError(fmt::format("table \"{}\" requires {} field{}, row {} has {} fields", table->name(),
                           table->columnSpecifiers().size(), table->columnSpecifiers().size() == 1 ? "" : "s",
                           rowIndex + 1, row.size()));
                return {};
            }

            for (std::size_t fieldIndex = 0; fieldIndex < row.size(); ++fieldIndex) {
                const auto &typeSpecifier = table->columnSpecifiers()[fieldIndex].typeSpecifier();

                // Type conversion check
                if (!row[fieldIndex].canConvertTo(table->columnSpecifiers()[fieldIndex].typeSpecifier())) {
                    postQueryError(fmt::format(R"(value "{}" for field `{}` of row {} isn't or cannot be converted to type {})",
                               row[fieldIndex].toString(), table->columnSpecifiers()[fieldIndex].name(), rowIndex + 1,
                               typeSpecifier.toString()));
                    return {};
                }

                // Max length check
                switch (typeSpecifier.typeName()) {
                    case TypeName::CHAR: {
                        auto string = row[fieldIndex].asString();
                        if (string.length() > typeSpecifier.maxSize()) {
                            postQueryError(fmt::format(R"(value "{}" for field `{}` of row {} is too long: {} characters > {})",
                                                       string, table->columnSpecifiers()[fieldIndex].name(), rowIndex + 1, string.length(),
                                                       typeSpecifier.toString()));
                            return {};
                        }
                        break;
                    }
                    case TypeName::INTEGER:
                        // Max length isn't appropriate here
                        break;
                    case TypeName::VARCHAR: {
                        auto string = row[fieldIndex].asString();
                        if (string.length() > typeSpecifier.maxSize()) {
                            postQueryError(fmt::format(R"(value "{}" for field `{}` of row {} is too long: {} characters > {})",
                                       string, table->columnSpecifiers()[fieldIndex].name(), rowIndex + 1, string.length(),
                                       typeSpecifier.toString()));
                            return {};
                        }
                        break;
                    }
                }
            }
        }

        for (ColumnContainer &container : table->columnContainers()) {
            container.reserve(std::size(query->values()));
        }

        for (auto &row : query->values()) {
            for (std::size_t columnIndex = 0; columnIndex < std::size(row); ++columnIndex) {
                const auto &specifier = table->columnSpecifiers()[columnIndex];
                auto &container = table->columnContainers()[columnIndex];
                switch (specifier.typeSpecifier().typeName()) {
                    case TypeName::CHAR: {
                        std::string value = row[columnIndex].asString();
                        value.resize(specifier.typeSpecifier().maxSize(), ' ');
                        std::get<FixedStringVector>(container).append(value);
                        break;
                    }
                    case TypeName::INTEGER:
                        std::get<std::vector<std::int64_t>>(container).push_back(row[columnIndex].asInteger());
                        break;
                    case TypeName::VARCHAR:
                        std::get<std::vector<std::string>>(container).push_back(row[columnIndex].asString());
                        break;
                }
            }
        }

        postInformationalMessage(fmt::format("Inserted {} row{} in table \"{}\"", query->values().size(),
                query->values().size() == 1 ? "" : "s", table->name()));
        return {};
    }

    TableViewPresentation
    Strawberry::processQuery(std::string_view input) {
        auto query = m_parser.parseQuery(input);
        if (query == nullptr)
            return {};

        StaticAnalyzer staticAnalyzer{this, m_messageInterface};
        if (staticAnalyzer.analyze(query.get()) == StaticAnalysisResult::ERRONEOUS) {
            return {};
        }

        switch (query->type()) {
            case Query::Type::CREATE_TABLE:
                return processCreateTableQuery(downcast<CreateTableQuery *>(query.get()));
            case Query::Type::DESCRIBE:
                return processDescribeQuery(downcast<DescribeQuery *>(query.get()));
            case Query::Type::INSERT_INTO:
                return processInsertIntoQuery(downcast<InsertIntoQuery *>(query.get()));
            case Query::Type::SELECT_FROM:
                return processSelectFromQuery(downcast<SelectFromQuery *>(query.get()));
            case Query::Type::SHOW_DATABASES:
                return processShowDatabasesQuery();
            case Query::Type::SHOW_TABLES:
                return processShowTablesQuery(downcast<ShowTablesQuery *>(query.get()));
        }

        postQueryError(fmt::format("parser return unknown query of type: {}", query->type()));
        return {};
    }

    [[nodiscard]] const Aliasable<TableColumnSpecifier> *
    whichColumnAnySpecifierMatches(const Table *table, std::string_view columnName,
                                   const std::vector<Aliasable<TableColumnSpecifier>> &specifiers) {
        for (const auto &specifier : specifiers) {
            if (specifier.original().columnName() == "*") {
                if (specifier.original().tableName().empty())
                    return &specifier;
                if (stringEqualsIgnoreCase(specifier.original().tableName(), table->name()))
                    return &specifier;

                continue;
            }

            if (specifier.original().tableName().empty() || stringEqualsIgnoreCase(specifier.original().tableName(), table->name())) {
                if (stringEqualsIgnoreCase(specifier.original().columnName(), columnName))
                    return &specifier;
            }
        }

        return nullptr;
    }

    struct AggregationOutputChannel {
        std::vector<ColumnSpecifier> specifiers{};
        std::vector<Strawberry::SimpleRow> aggregatedRows{};

        inline void
        fillInconsistencyWithNull() {
            // Fill inconsistent column counts with NULLs
            // For that we first have to calculate the max row count
            std::size_t maxColumnCount = 0;
            for (const auto &row : aggregatedRows) {
                maxColumnCount = std::max(maxColumnCount, std::size(row));
            }

            for (auto &row : aggregatedRows) {
                row.resize(maxColumnCount, Variable{kNull});
            }
        }
    };

    static void
    collectTableColumnInChannel(AggregationOutputChannel &channel, const Table *const table, std::size_t columnIndex,
                                std::string_view aliasName) {
        if (std::empty(aliasName)) {
            channel.specifiers.push_back(table->columnSpecifiers()[columnIndex]);
        } else {
            channel.specifiers.emplace_back(std::string(aliasName), table->columnSpecifiers()[columnIndex].typeSpecifier());
        }

        const auto &container = table->columnContainers()[columnIndex];

        for (std::size_t rowIndex = 0; rowIndex < container.size(); ++rowIndex) {
            Strawberry::SimpleRow *row;

            if (channel.aggregatedRows.size() == rowIndex) {
                row = &channel.aggregatedRows.emplace_back();
            } else if (channel.aggregatedRows.size() < rowIndex) {
                channel.aggregatedRows.resize(rowIndex + 1);
                row = &channel.aggregatedRows.back();
            } else {
                row = &channel.aggregatedRows[rowIndex];
            }

            row->push_back(container[rowIndex]);
        }
    }

    Strawberry::AggregatedRowsResult
    Strawberry::aggregateRows(const std::map<std::string_view, Table *> &tables, const SelectFromQuery *query,
                              AggregationByProductInclude includeByProducts) {
        AggregationOutputChannel defaultChannel{};
        AggregationOutputChannel byProductChannel{};

        for (const auto &tableEntry : tables) {
            const Table *const table = tableEntry.second;
            for (std::size_t tableColumnIndex = 0; tableColumnIndex < std::size(table->columnSpecifiers()); ++tableColumnIndex) {
                const auto *specifier = whichColumnAnySpecifierMatches(table, table->columnSpecifiers()[tableColumnIndex].name(),
                                                                       query->columns());
                if (specifier != nullptr) {
                    collectTableColumnInChannel(defaultChannel, table, tableColumnIndex, specifier->aliasName());
                } else if (includeByProducts == AggregationByProductInclude::Yes) {
                    collectTableColumnInChannel(byProductChannel, table, tableColumnIndex, {});
                }
            }
        }

        defaultChannel.fillInconsistencyWithNull();
        if (includeByProducts == AggregationByProductInclude::Yes)
            byProductChannel.fillInconsistencyWithNull();

        return {std::move(defaultChannel.specifiers), std::move(defaultChannel.aggregatedRows),
                std::move(byProductChannel.specifiers), std::move(byProductChannel.aggregatedRows)};
    }

    TableViewPresentation
    Strawberry::processSelectFromQuery(SelectFromQuery *query) {
        if (m_currentDatabase == nullptr) {
            postQueryError(fmt::format("cannot select from a table when no database is selected."));
            return {};
        }

        std::map<std::string_view, Table *> tables{};

        for (const auto &tableName : query->tables()) {
            auto *table = m_currentDatabase->tableByName(tableName.original());
            if (table == nullptr) {
                postQueryError(fmt::format("table \"{}\" does not exist.", tableName.original()));
                return {};
            }

            if (tables.find(table->name()) != std::cend(tables)) {
                if (tableName.hasAlias())
                    postQueryError(fmt::format("alias \"{}\" occurred in SELECT multiple times.", tableName.aliasName()));
                else
                    postQueryError(fmt::format("table \"{}\" selected multiple times.", tableName.original()));
                return {};
            }

            if (tableName.hasAlias())
                tables.emplace(std::string_view{tableName.aliasName()}, table);
            else
                tables.emplace(std::string_view{table->name()}, table);
        }

        auto aggregation = aggregateRows(tables, query, query->whereExpression() ? AggregationByProductInclude::Yes
                                                                                 : AggregationByProductInclude::No);
        if (std::empty(aggregation.rows))
            return {};

        TableViewPresentation tableViewPresentation{};
        if (std::size(aggregation.specifiers) != std::size(aggregation.rows[0])) {
            postQueryError("Internal Error: specifier count is not equal to the first row's column count");
            return {};
        }

        const auto columnCount = std::size(aggregation.specifiers);
        tableViewPresentation.columns().resize(columnCount);

        for (std::size_t columnIndex = 0; columnIndex < columnCount; ++columnIndex) {
            auto &column = tableViewPresentation.columns()[columnIndex];
            column.reserve(1 + columnCount);

            column.push_back(aggregation.specifiers[columnIndex].name());
        }

        // The final vector of rows that passed the WHERE filtering.
        using CollectedRowType = std::vector<Variable>;
        std::vector<CollectedRowType> collectedRows{};

        if (!query->whereExpression()) {
            collectedRows = std::move(aggregation.rows);
        } else {
            collectedRows.reserve(std::size(aggregation.rows));
            for (std::size_t rowIndex = 0; rowIndex < std::size(aggregation.rows); ++rowIndex) {
                Row row{aggregation.specifiers, std::move(aggregation.rows[rowIndex])};
                Row byProductRow = aggregation.byProductRows.size() > rowIndex
                                 ? Row{aggregation.byProductSpecifiers, std::move(aggregation.byProductRows[rowIndex])}
                                 : Row{{}, {}};
                const auto whereResult = invokeWhereClauseOnRow(row, byProductRow, query->whereExpression().get());
                switch (whereResult) {
                    case SubroutineResult::INTERNAL_ERROR:
                    case SubroutineResult::ERROR:
                        return {};
                    case SubroutineResult::FALSE:
                        continue;
                    case SubroutineResult::TRUE:
                        collectedRows.push_back(std::move(row.columns()));
                        break;
                }
            }
        }

        if (std::size(query->orderBySpecifiers()) > 1) {
            postQueryError(fmt::format("UNIMPLEMENTED: zero or one ORDER BY are only supported, not {}",
                                       std::size(query->orderBySpecifiers())));
            return {};
        }

        if (std::size(query->orderBySpecifiers()) == 1) {
            const auto &order = query->orderBySpecifiers().front();
            auto columnIndex = [&order, &aggregation] () -> std::optional<std::size_t> {
                for (std::size_t i = 0; i < std::size(aggregation.specifiers); ++i) {
                    if (aggregation.specifiers[i].name() == order.tableColumnSpecifier().columnName())
                        return i;
                }
                return std::nullopt;
            }();

            if (!columnIndex) {
                postQueryError(fmt::format("unknown column in ORDER BY clause: {}",
                                           order.tableColumnSpecifier().asFormattedString()));
                return {};
            }

            auto ascendingComparator = [columnIndex = columnIndex.value()] (const CollectedRowType &a, const CollectedRowType &b) {
                return a[columnIndex] < b[columnIndex];
            };

            auto descendingComparator = [columnIndex = columnIndex.value()] (const CollectedRowType &a, const CollectedRowType &b) {
                return a[columnIndex] > b[columnIndex];
            };

            switch (order.type()) {
                case OrderBySpecifier::Type::ASCENDING:
                    std::sort(std::begin(collectedRows), std::end(collectedRows), ascendingComparator);
                    break;
                case OrderBySpecifier::Type::DESCENDING:
                    std::sort(std::begin(collectedRows), std::end(collectedRows), descendingComparator);
                    break;
            }
        }

        for (std::size_t rowIndex = 0; rowIndex < std::size(collectedRows); ++rowIndex) {
            for (std::size_t columnIndex = 0; columnIndex < std::size(collectedRows[rowIndex]); ++columnIndex) {
                auto &column = tableViewPresentation.columns()[columnIndex];
                auto value = collectedRows[rowIndex].at(columnIndex).toString();
                const auto indexInDestinationColumn = rowIndex + 1;
                if (column.size() == indexInDestinationColumn) {
                    column.push_back(std::move(value));
                } else {
                    if (column.size() < indexInDestinationColumn)
                        column.resize(indexInDestinationColumn + 1);
                    column[indexInDestinationColumn] = std::move(value);
                }
            }
        }

        return tableViewPresentation;
    }

    TableViewPresentation
    Strawberry::processShowDatabasesQuery() {
        TableViewPresentation tableViewPresentation{};

        tableViewPresentation.columns().emplace_back();
        tableViewPresentation.columns().front().reserve(1 + std::size(m_databases));
        tableViewPresentation.columns().front().emplace_back("Name");

        for (std::size_t i = 0; i < std::size(m_databases); ++i) {
            tableViewPresentation.columns().front().emplace_back(m_databases[i].name());
        }

        return tableViewPresentation;
    }

    TableViewPresentation
    Strawberry::processShowTablesQuery(ShowTablesQuery *query) {
        Database *database;

        if (std::empty(m_databases)) {
            postQueryError("cannot select show tables when there are no databases");
            return {};
        }

        if (std::empty(query->databaseName())) {
            if (m_currentDatabase == nullptr) {
                postQueryError(fmt::format("cannot list tables when no database is currently selected."));
                return {};
            }
            database = m_currentDatabase;
        } else {
            database = databaseByName(query->databaseName());
            if (!database) {
                auto suggestedName = getClosestDatabaseName(query->databaseName());
                if (suggestedName.empty())
                    postQueryError(fmt::format(R"(unknown database named "{}")", query->databaseName()));
                else
                    postQueryError(fmt::format(R"(unknown database named "{}", did you mean "{}"?)",
                                   query->databaseName(), suggestedName));
                return {};
            }
        }

        TableViewPresentation tableViewPresentation{};

        tableViewPresentation.columns().emplace_back();
        tableViewPresentation.columns().front().reserve(1 + std::size(database->tables()));
        tableViewPresentation.columns().front().emplace_back("Name");

        for (std::size_t i = 0; i < std::size(database->tables()); ++i) {
            tableViewPresentation.columns().front().emplace_back(database->tables()[i].name());
        }

        return tableViewPresentation;
    }

} // namespace strawberry
