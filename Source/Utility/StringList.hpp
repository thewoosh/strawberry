/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <optional>
#include <string>
#include <vector>

namespace strawberry {

    [[nodiscard]] const std::string *
    getClosestMatchingString(const std::vector<std::string> &strings, std::string_view input);

    [[nodiscard]] const std::string_view *
    getClosestMatchingString(const std::vector<std::string_view> &strings, std::string_view input);

} // namespace strawberry
