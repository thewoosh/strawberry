/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/Utility/Conversions.hpp"

namespace strawberry {

    std::optional<std::int64_t>
    convertStringToInteger(std::string_view input) {
        const char *start = std::begin(input);
        char *end = const_cast<char *>(std::end(input));

        auto value = static_cast<std::int64_t>(std::strtoll(start, &end, 10));
        if (end == std::end(input))
            return value;

        return std::nullopt;
    }

} // namespace strawberry
