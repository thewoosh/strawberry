/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string_view>

namespace strawberry {

    [[nodiscard]] inline constexpr bool
    isASCIIAlpha(char character) {
        return (character >= 'A' && character <= 'Z') || (character >= 'a' && character <= 'z');
    }

    [[nodiscard]] inline constexpr bool
    isDigit(char character) {
        return character >= '0' && character <= '9';
    }

    [[nodiscard]] inline constexpr bool
    isIdentifierStart(char character) {
        return isASCIIAlpha(character);
    }

    [[nodiscard]] inline constexpr bool
    isIdentifierContinue(char character) {
        return isIdentifierStart(character) || isDigit(character) || character == '_';
    }

    /**
     * Makes the character lower cased if it is a ASCII capital letter (basic
     * latin A-Z), otherwise will just return the input character.
     */
    template<typename Type>
    [[nodiscard]] inline constexpr Type
    toASCIILowercase(Type c) {
        if (c >= 'a' && c <= 'z')
            return static_cast<Type>(c - ('a' - 'A'));
        return c;
    }

    /**
     * Makes the character upper cased if it is a ASCII capital letter (basic
     * latin a-Zz), otherwise will just return the input character.
     */
    template<typename Type>
    [[nodiscard]] inline constexpr Type
    toASCIIUppercase(Type c) {
        if (c >= 'A' && c <= 'Z')
            return static_cast<Type>(c + ('a' - 'A'));
        return c;
    }

} // namespace strawberry
