/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "ThirdParty/Serenity/FuzzyMatch.hpp"

namespace strawberry {

    const std::string *
    getClosestMatchingString(const std::vector<std::string> &strings, std::string_view input) {
        std::vector<int> results(strings.size());
        for (std::size_t i = 0; i < strings.size(); ++i) {
            auto result = serenity::fuzzyMatch(input, strings[i]);
            if (result.matched)
                results[i] = result.score;
            else
                results[i] = -1;
        }
        auto it = std::max_element(std::cbegin(results), std::cend(results));
        return &strings[static_cast<std::size_t>(std::distance(std::cbegin(results), it))];
    }

    [[nodiscard]] const std::string_view *
    getClosestMatchingString(const std::vector<std::string_view> &strings, std::string_view input) {
        std::vector<int> results(strings.size());
        for (std::size_t i = 0; i < strings.size(); ++i) {
            auto result = serenity::fuzzyMatch(input, strings[i]);
            if (result.matched)
                results[i] = result.score;
            else
                results[i] = -1;
        }
        auto it = std::max_element(std::cbegin(results), std::cend(results));
        return &strings[static_cast<std::size_t>(std::distance(std::cbegin(results), it))];
    }

} // namespace strawberry
