/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <type_traits>

namespace strawberry {

    template <typename Enum>
    [[nodiscard]] inline constexpr auto
    toUnderlying(Enum e) {
        return static_cast<std::underlying_type_t<Enum>>(e);
    }

} // namespace strawberry
