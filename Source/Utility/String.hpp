/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include "Source/Utility/Characters.hpp"

#include <string>
#include <string_view>

namespace strawberry {

    [[nodiscard]] constexpr bool
    stringEqualsIgnoreCase(std::string_view a, std::string_view b) {
        if (a.length() != b.length())
            return false;
        for (std::size_t i = 0; i < a.length(); ++i) {
            if (toASCIILowercase(a[i]) != toASCIILowercase(b[i]))
                return false;
        }
        return true;
    }

    template <typename StringType>
    [[nodiscard]] inline constexpr auto
    trim(StringType &&string) {
        constexpr const char *const match = " \t\n\r\f\v";
        const auto begin = std::min(string.find_first_not_of(match), string.length());
        return string.substr(begin, string.find_last_not_of(match) + 1 - begin);
    }

} // namespace strawberry
