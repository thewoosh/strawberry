/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <memory>

#include "Source/Expression/Expression.hpp"

namespace strawberry {

    struct BiExpression final
            : public Expression {

        [[nodiscard]] inline
        BiExpression(Expression::Type type, std::unique_ptr<Expression> &&lhs, std::unique_ptr<Expression> &&rhs)
                : Expression(type)
                , m_lhs(std::move(lhs))
                , m_rhs(std::move(rhs)) {
        }

        [[nodiscard]] inline std::unique_ptr<Expression> &
        lhs() {
            return m_lhs;
        }

        [[nodiscard]] inline const std::unique_ptr<Expression> &
        lhs() const {
            return m_lhs;
        }

        [[nodiscard]] inline std::unique_ptr<Expression> &
        rhs() {
            return m_rhs;
        }

        [[nodiscard]] inline const std::unique_ptr<Expression> &
        rhs() const {
            return m_rhs;
        }

    private:
        std::unique_ptr<Expression> m_lhs;
        std::unique_ptr<Expression> m_rhs;
    };

} // namespace strawberry
