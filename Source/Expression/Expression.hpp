/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

namespace strawberry {

    /**
     * The generic underlying type of all expressions.
     */
    struct Expression {
        enum class Type {
            COMPARE_EQUAL_TO,
            COMPARE_GREATER_THAN,
            COMPARE_GREATER_THAN_OR_EQUAL,
            COMPARE_LESS_THAN,
            COMPARE_LESS_THAN_OR_EQUAL,
            COMPARE_NOT_EQUAL_TO,
            LIKE,
            PRIMARY_IDENTIFIER,
            PRIMARY_INTEGER,
            PRIMARY_STRING,
        };

        [[nodiscard]] inline constexpr explicit
        Expression(Type type)
                : m_type(type) {
        }

        virtual
        ~Expression() = default;

        [[nodiscard]] inline constexpr Type
        type() const {
            return m_type;
        }

    private:
        const Type m_type;
    };

} // namespace strawberry
