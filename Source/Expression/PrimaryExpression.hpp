/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include "Source/Expression/Expression.hpp"

namespace strawberry {

    template <typename Contained>
    struct PrimaryExpression final
            : public Expression {

        [[nodiscard]] inline constexpr
        PrimaryExpression(Expression::Type type, Contained data)
                : Expression(type)
                , m_contained(std::move(data)) {
        }

        [[nodiscard]] inline Contained &
        data() {
            return m_contained;
        }

        [[nodiscard]] inline const Contained &
        data() const {
            return m_contained;
        }

    private:
        Contained m_contained;
    };

} // namespace strawberry
