/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string>

#include "Source/Expression/Expression.hpp"
#include "Source/Syntax/TableColumnQualifier.hpp"

namespace strawberry {

    struct LikeExpression final
            : public Expression {
        [[nodiscard]] inline
        LikeExpression(TableColumnSpecifier &&tableColumnSpecifier, std::string &&matchString)
                : Expression(Expression::Type::LIKE)
                , m_tableColumnSpecifier(std::move(tableColumnSpecifier))
                , m_matchString(std::move(matchString)) {
        }

        [[nodiscard]] inline TableColumnSpecifier &
        tableColumnSpecifier() {
            return m_tableColumnSpecifier;
        }

        [[nodiscard]] inline const TableColumnSpecifier &
        tableColumnSpecifier() const {
            return m_tableColumnSpecifier;
        }

        [[nodiscard]] inline std::string &
        matchString() {
            return m_matchString;
        }

        [[nodiscard]] inline const std::string &
        matchString() const {
            return m_matchString;
        }

    private:
        TableColumnSpecifier m_tableColumnSpecifier;
        std::string m_matchString;
    };

} // namespace strawberry
