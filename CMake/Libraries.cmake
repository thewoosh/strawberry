# SPDX-License-Identifier: BSD-2-Clause
#
# Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
# All Rights Reserved.

option(LIBRARY_FMT_NON_LOCAL "Don't use the system's {fmt} library" OFF)
option(LIBRARY_GOOGLETEST_NON_LOCAL "Don't use the system's GoogleTest library" OFF)

if (NOT LIBRARY_FMT_NON_LOCAL)
    find_package(fmt QUIET)
endif()

if (${LIBRARY_FMT_NON_LOCAL} OR NOT ${fmt_FOUND})
    add_subdirectory(${CMAKE_SOURCE_DIR}/ThirdParty/fmt)
endif()

if (${ENABLE_TESTING})
    if (NOT ${LIBRARY_GOOGLETEST_NON_LOCAL})
        find_package(GTest 1.10.0 QUIET)
    endif()

    if (${LIBRARY_GOOGLETEST_NON_LOCAL} OR NOT ${GTest_FOUND})
        add_subdirectory(${CMAKE_SOURCE_DIR}/ThirdParty/GoogleTest)
    endif()
endif()
