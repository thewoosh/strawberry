# SPDX-License-Identifier: BSD-2-Clause
#
# Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
# All Rights Reserved.

set(VERSION_MAJOR 2021)
set(VERSION_MINOR 10)
set(VERSION_PATCH 0)

add_compile_definitions(STRAWBERRY_VERSION_MAJOR=${VERSION_MAJOR}
						STRAWBERRY_VERSION_MINOR=${VERSION_MINOR}
						STRAWBERRY_VERSION_PATCH=${VERSION_PATCH})
