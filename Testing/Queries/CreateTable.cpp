/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Testing/Base.hpp"

#include <fmt/format.h>

#include "Source/Strawberry.hpp"
#include "Testing/MessageSink.hpp"

using namespace strawberry;

TEST(Queries_CreateTable, SimpleTest) {
    Strawberry instance{kStandardMessageInterface};

    ASSERT_NE(instance.currentDatabase(), nullptr);
    const auto result = instance.processQuery("CREATE TABLE test (number INTEGER);");

    ASSERT_EQ(std::size(instance.currentDatabase()->tables()), 1);

    const Table &table = instance.currentDatabase()->tables().front();
    EXPECT_EQ(table.name(), "test");

    ASSERT_EQ(table.columnSpecifiers().size(), 1);
    EXPECT_EQ(table.columnSpecifiers().front().name(), "number");
    EXPECT_EQ(table.columnSpecifiers().front().typeSpecifier().maxSize(), 0);
    EXPECT_EQ(table.columnSpecifiers().front().typeSpecifier().typeName(), TypeName::INTEGER);
}

TEST(Queries_CreateTable, VarcharTest) {
    Strawberry instance{kStandardMessageInterface};

    ASSERT_NE(instance.currentDatabase(), nullptr);
    const auto result = instance.processQuery("CREATE TABLE test (string VARCHAR(50));");

    ASSERT_EQ(std::size(instance.currentDatabase()->tables()), 1);

    const Table &table = instance.currentDatabase()->tables().front();
    EXPECT_EQ(table.name(), "test");

    ASSERT_EQ(table.columnSpecifiers().size(), 1);
    EXPECT_EQ(table.columnSpecifiers().front().name(), "string");
    EXPECT_EQ(table.columnSpecifiers().front().typeSpecifier().maxSize(), 50);
    EXPECT_EQ(table.columnSpecifiers().front().typeSpecifier().typeName(), TypeName::VARCHAR);
}

TEST(Queries_CreateTable, SameName) {
    MessageSink messageSink{};
    Strawberry instance{messageSink.createInterface()};

    ASSERT_NE(instance.currentDatabase(), nullptr);
    auto result = instance.processQuery("CREATE TABLE test (string VARCHAR(50));");
    ASSERT_EQ(std::size(instance.currentDatabase()->tables()), 1);

    for (std::string_view name : {"test", "Test", "TEST", "tEsT"}) {
        messageSink.clear();

        result = instance.processQuery(fmt::format("CREATE TABLE {} (string VARCHAR(5));", name));
        EXPECT_TRUE(result.columns().empty());
        EXPECT_TRUE(std::empty(messageSink.lexerErrors()));
        EXPECT_TRUE(std::empty(messageSink.parserErrors()));
        EXPECT_TRUE(std::empty(messageSink.informationMessages()));
        ASSERT_EQ(std::size(messageSink.queryErrors()), 1);
        constexpr std::string_view prefix = "a table named \"";
        const std::string_view queryError = messageSink.queryErrors().back();
        EXPECT_TRUE(queryError.starts_with(prefix)) << "Doesn't start with prefix: '" << prefix << '\'';
        EXPECT_EQ(queryError.substr(prefix.size(), name.length()), name);
        EXPECT_TRUE(queryError.ends_with("\" already exists in database \"main\""));
    }
}
