/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Testing/Base.hpp"

#include "Source/Strawberry.hpp"

using namespace strawberry;

Strawberry instance{kStandardMessageInterface};

TEST(Queries_SelectFromBasic, Initialization) {
    const auto createTableResult = instance.processQuery("CREATE TABLE TestTable (number INTEGER, string VARCHAR(50);");
    EXPECT_TRUE(createTableResult.columns().empty());
    const auto insertResult = instance.processQuery("INSERT INTO TestTable VALUES (1, 2), (3, 4), (5, 6), (7, 8);");
    EXPECT_TRUE(createTableResult.columns().empty());
}

TEST(Queries_SelectFromBasic, AsteriskTest) {
    const auto result = instance.processQuery("SELECT * FROM TestTable;");
    ASSERT_EQ(result.columns().size(), 2);
    std::vector<std::string_view> column1{"number", "1", "3", "5", "7"};
    std::vector<std::string_view> column2{"string", "2", "4", "6", "8"};
    COMPARE_CONTAINERS(result.columns()[0], column1);
    COMPARE_CONTAINERS(result.columns()[1], column2);
}

TEST(Queries_SelectFromBasic, TableNameAliasWithoutASTest) {
    const auto result = instance.processQuery("SELECT * FROM TestTable CoolTableName;");
    ASSERT_EQ(result.columns().size(), 2);
    std::vector<std::string_view> column1{"number", "1", "3", "5", "7"};
    std::vector<std::string_view> column2{"string", "2", "4", "6", "8"};
    COMPARE_CONTAINERS(result.columns()[0], column1);
    COMPARE_CONTAINERS(result.columns()[1], column2);
}

TEST(Queries_SelectFromBasic, TableNameAliasWithASTest) {
    const auto result = instance.processQuery("SELECT * FROM TestTable AS CoolTableName;");
    ASSERT_EQ(result.columns().size(), 2);
    std::vector<std::string_view> column1{"number", "1", "3", "5", "7"};
    std::vector<std::string_view> column2{"string", "2", "4", "6", "8"};
    COMPARE_CONTAINERS(result.columns()[0], column1);
    COMPARE_CONTAINERS(result.columns()[1], column2);
}

TEST(Queries_SelectFromBasic, ColumnNameAliasTest) {
    const auto result = instance.processQuery("SELECT number CoolNumber, string WeirdString FROM TestTable;");
    ASSERT_EQ(result.columns().size(), 2);
    std::vector<std::string_view> column1{"CoolNumber", "1", "3", "5", "7"};
    std::vector<std::string_view> column2{"WeirdString", "2", "4", "6", "8"};
    COMPARE_CONTAINERS(result.columns()[0], column1);
    COMPARE_CONTAINERS(result.columns()[1], column2);
}
