/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Testing/Base.hpp"

#include "Source/Strawberry.hpp"

using namespace strawberry;

Strawberry instance{kStandardMessageInterface};

TEST(Queries_SelectFromOrderBy, Initialization) {
    static_cast<void>(instance.processQuery("CREATE TABLE NumberTable (id INTEGER);"));
    static_cast<void>(instance.processQuery("INSERT INTO NumberTable VALUES (5), (1), (2), (0);"));
    static_cast<void>(instance.processQuery("INSERT INTO NumberTable VALUES (6), (8), (2), (3);"));
    static_cast<void>(instance.processQuery("CREATE TABLE StringTable (id VARCHAR(50));"));
    static_cast<void>(instance.processQuery("INSERT INTO StringTable VALUES ('hello'), ('world'), ('goodbye'), ('cruel');"));
    static_cast<void>(instance.processQuery("INSERT INTO StringTable VALUES ('Hello'), ('World'), ('Goodbye'), ('Cruel');"));
}

TEST(Queries_SelectFromOrderBy, WithoutTypeKeyword) {
    const auto result = instance.processQuery("SELECT * FROM NumberTable ORDER BY id;");
    ASSERT_EQ(result.columns().size(), 1);

    std::vector<std::string_view> column{"id", "0", "1", "2", "2", "3", "5", "6", "8"};
    COMPARE_CONTAINERS(result.columns()[0], column);
}

TEST(Queries_SelectFromOrderBy, NumberAscending) {
    const auto result = instance.processQuery("SELECT * FROM NumberTable ORDER BY id ASC;");
    ASSERT_EQ(result.columns().size(), 1);

    std::vector<std::string_view> column{"id", "0", "1", "2", "2", "3", "5", "6", "8"};
    COMPARE_CONTAINERS(result.columns()[0], column);
}

TEST(Queries_SelectFromOrderBy, NumberDescending) {
    const auto result = instance.processQuery("SELECT * FROM NumberTable ORDER BY id DESC;");
    ASSERT_EQ(result.columns().size(), 1);

    std::vector<std::string_view> column{"id", "8", "6", "5", "3", "2", "2", "1", "0"};
    COMPARE_CONTAINERS(result.columns()[0], column);
}

TEST(Queries_SelectFromOrderBy, StringAscending) {
    const auto result = instance.processQuery("SELECT * FROM StringTable ORDER BY id ASC;");
    ASSERT_EQ(result.columns().size(), 1);

    std::vector<std::string_view> column{"id", "Cruel", "Goodbye", "Hello", "World", "cruel", "goodbye", "hello", "world"};
    COMPARE_CONTAINERS(result.columns()[0], column);
}

TEST(Queries_SelectFromOrderBy, StringDescending) {
    const auto result = instance.processQuery("SELECT * FROM StringTable ORDER BY id DESC;");
    ASSERT_EQ(result.columns().size(), 1);

    std::vector<std::string_view> column{"id", "world", "hello", "goodbye", "cruel", "World", "Hello", "Goodbye", "Cruel"};
    COMPARE_CONTAINERS(result.columns()[0], column);
}
