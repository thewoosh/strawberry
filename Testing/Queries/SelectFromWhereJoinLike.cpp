/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Testing/Base.hpp"

#include "Source/Strawberry.hpp"

using namespace strawberry;

Strawberry instance{kStandardMessageInterface};

TEST(Queries_SelectFromWhereJoinLike, Initialization) {
    static_cast<void>(instance.processQuery("CREATE TABLE LeftTable (lid INTEGER);"));
    static_cast<void>(instance.processQuery("INSERT INTO LeftTable VALUES (1), (5), (7), (6);"));
    static_cast<void>(instance.processQuery("CREATE TABLE RightTable (rid INTEGER, age INTEGER);"));
    static_cast<void>(instance.processQuery("INSERT INTO RightTable VALUES (0, 29), (5, 30), (7, 16), (6, 78);"));
}

TEST(Queries_SelectFromWhereJoinLike, Simple) {
    const auto result = instance.processQuery("SELECT age FROM LeftTable, RightTable WHERE lid = rid;");
    ASSERT_EQ(result.columns().size(), 1);

    std::vector<std::string_view> column{"age", "30", "16", "78"};
    COMPARE_CONTAINERS(result.columns()[0], column);
}
