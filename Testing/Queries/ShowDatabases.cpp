/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Testing/Base.hpp"

#include "Source/Strawberry.hpp"

using namespace strawberry;

TEST(Queries_ShowDatabases, ThereShouldBeADefaultDatabase) {
    Strawberry instance{kStandardMessageInterface};
    ASSERT_NE(instance.currentDatabase(), nullptr);
    EXPECT_EQ(std::size(instance.databases()), 1);

    const auto result = instance.processQuery("SHOW DATABASES;");
    ASSERT_EQ(std::size(result.columns()), 1);
    ASSERT_EQ(std::size(result.columns()[0]), 2);
    EXPECT_EQ(result.columns()[0][0], "Name");
    EXPECT_EQ(result.columns()[0][1], instance.currentDatabase()->name());
}

TEST(Queries_ShowDatabases, MultipleDatabases) {
    Strawberry instance{kStandardMessageInterface};
    ASSERT_NE(instance.currentDatabase(), nullptr);
    EXPECT_EQ(std::size(instance.databases()), 1);

    constexpr const char *const newDatabaseName = "UnitTestDatabaseName";
    instance.databases().emplace_back(newDatabaseName);

    // NOTE: this might break if the order of databases inserted and retrieved is changed

    const auto result = instance.processQuery("SHOW DATABASES;");
    ASSERT_EQ(std::size(result.columns()), 1);
    ASSERT_EQ(std::size(result.columns()[0]), 3);
    ASSERT_EQ(result.columns()[0][0], "Name");
    EXPECT_EQ(result.columns()[0][1], instance.databases()[0].name());
    EXPECT_EQ(result.columns()[0][2], instance.databases()[1].name());
}
