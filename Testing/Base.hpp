/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <gtest/gtest.h>

#include "Source/Interface/MessageInterface.hpp"

using namespace std::literals;

const strawberry::MessageInterface kStandardMessageInterface = strawberry::MessageInterface::createFatalError([] () {
    FAIL() << "Message posted to MessageInterface pump, which is fatal";
});

extern "C" {
    void __ubsan_on_report() {
        FAIL() << "Encountered an undefined behavior sanitizer error";
    }
    void __asan_on_error() {
        FAIL() << "Encountered an address sanitizer error";
    }
    void __tsan_on_report() {
        FAIL() << "Encountered a thread sanitizer error";
    }
}  // extern "C"


/**
 * Defines how many samples we should take when using random input values.
 */
static constexpr const std::size_t RANDOM_SAMPLES = 16;

#define COMPARE_CONTAINERS(container1, container2) \
    ASSERT_EQ(std::size(container1), std::size(container2)); \
    for (std::size_t _ccIndex = 0; _ccIndex < std::size(container1); ++_ccIndex) \
        EXPECT_EQ((container1)[_ccIndex], (container2)[_ccIndex]) << "Mismatch at index " << _ccIndex

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
