/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Testing/Base.hpp"

#include <fmt/format.h>

#define STRAWBERRY_INTERNAL_VISIBILITY public
#include "Source/Strawberry.hpp"

#include "Source/Expression/BiExpression.hpp"
#include "Source/Expression/PrimaryExpression.hpp"

using namespace strawberry;

Strawberry instance{kStandardMessageInterface};

struct Entry {
    bool shouldPass;
    std::int64_t a;
    std::int64_t b;
};

TEST(Expressions_IntegerComparisons, GreaterThan) {
    constexpr const std::array<Entry, 12> entries{{
        {false, 0, 0},
        {false, 628612961, 628612961},
        {true, 1, 0},
        {false, 0, 1},
        {true, std::numeric_limits<std::int64_t>::max(), 0},
        {false, std::numeric_limits<std::int64_t>::min(), 0},
        {false, 0, std::numeric_limits<std::int64_t>::max()},
        {true, 0, std::numeric_limits<std::int64_t>::min()},
        {true, std::numeric_limits<std::int64_t>::max(), std::numeric_limits<std::int64_t>::min()},
        {false, std::numeric_limits<std::int64_t>::min(), std::numeric_limits<std::int64_t>::max()},
        {false, std::numeric_limits<std::int64_t>::min(), std::numeric_limits<std::int64_t>::min()},
        {false, std::numeric_limits<std::int64_t>::max(), std::numeric_limits<std::int64_t>::max()},
    }};
    for (const auto &entry : entries) {
        BiExpression equalityExpression{
            Expression::Type::COMPARE_GREATER_THAN,
            std::make_unique<PrimaryExpression<std::int64_t>>(Expression::Type::PRIMARY_INTEGER, entry.a),
            std::make_unique<PrimaryExpression<std::int64_t>>(Expression::Type::PRIMARY_INTEGER, entry.b)
        };

        ASSERT_EQ(entry.a > entry.b, entry.shouldPass)
            << fmt::format("Incorrect test input of {} and {}", entry.a, entry.b);

        auto optionalVariable = instance.evaluateExpression(&equalityExpression, {});
        ASSERT_TRUE(optionalVariable.has_value());
        ASSERT_EQ(optionalVariable->type(), Variable::Type::BOOLEAN);
        EXPECT_EQ(optionalVariable->toBoolean(), entry.shouldPass)
                << (entry.shouldPass
                        ? fmt::format("{} should have been greater than {})", entry.a, entry.b)
                        : fmt::format("{} should not have been greater than {})", entry.a, entry.b));
    }
}

TEST(Expressions_IntegerComparisons, GreaterThanOrEqualTo) {
    constexpr const std::array<Entry, 12> entries{{
        {true, 0, 0},
        {true, 628612961, 628612961},
        {true, 1, 0},
        {false, 0, 1},
        {true, std::numeric_limits<std::int64_t>::max(), 0},
        {false, std::numeric_limits<std::int64_t>::min(), 0},
        {false, 0, std::numeric_limits<std::int64_t>::max()},
        {true, 0, std::numeric_limits<std::int64_t>::min()},
        {true, std::numeric_limits<std::int64_t>::max(), std::numeric_limits<std::int64_t>::min()},
        {false, std::numeric_limits<std::int64_t>::min(), std::numeric_limits<std::int64_t>::max()},
        {true, std::numeric_limits<std::int64_t>::min(), std::numeric_limits<std::int64_t>::min()},
        {true, std::numeric_limits<std::int64_t>::max(), std::numeric_limits<std::int64_t>::max()},
    }};
    for (const auto &entry : entries) {
        BiExpression equalityExpression{
            Expression::Type::COMPARE_GREATER_THAN_OR_EQUAL,
            std::make_unique<PrimaryExpression<std::int64_t>>(Expression::Type::PRIMARY_INTEGER, entry.a),
            std::make_unique<PrimaryExpression<std::int64_t>>(Expression::Type::PRIMARY_INTEGER, entry.b)
        };

        ASSERT_EQ(entry.a >= entry.b, entry.shouldPass)
            << fmt::format("Incorrect test input of {} and {}", entry.a, entry.b);

        auto optionalVariable = instance.evaluateExpression(&equalityExpression, {});
        ASSERT_TRUE(optionalVariable.has_value());
        ASSERT_EQ(optionalVariable->type(), Variable::Type::BOOLEAN);
        EXPECT_EQ(optionalVariable->toBoolean(), entry.shouldPass)
                << (entry.shouldPass
                        ? fmt::format("{} should have been greater than or equal to {})", entry.a, entry.b)
                        : fmt::format("{} should not have been greater than or equal to {})", entry.a, entry.b));
    }
}

TEST(Expressions_IntegerComparisons, LessThan) {
    constexpr const std::array<Entry, 12> entries{{
        {false, 0, 0},
        {false, 628612961, 628612961},
        {false, 1, 0},
        {true, 0, 1},
        {false, std::numeric_limits<std::int64_t>::max(), 0},
        {true, std::numeric_limits<std::int64_t>::min(), 0},
        {true, 0, std::numeric_limits<std::int64_t>::max()},
        {false, 0, std::numeric_limits<std::int64_t>::min()},
        {false, std::numeric_limits<std::int64_t>::max(), std::numeric_limits<std::int64_t>::min()},
        {true, std::numeric_limits<std::int64_t>::min(), std::numeric_limits<std::int64_t>::max()},
        {false, std::numeric_limits<std::int64_t>::min(), std::numeric_limits<std::int64_t>::min()},
        {false, std::numeric_limits<std::int64_t>::max(), std::numeric_limits<std::int64_t>::max()},
    }};
    for (const auto &entry : entries) {
        BiExpression equalityExpression{
            Expression::Type::COMPARE_LESS_THAN,
            std::make_unique<PrimaryExpression<std::int64_t>>(Expression::Type::PRIMARY_INTEGER, entry.a),
            std::make_unique<PrimaryExpression<std::int64_t>>(Expression::Type::PRIMARY_INTEGER, entry.b)
        };

        ASSERT_EQ(entry.a < entry.b, entry.shouldPass)
            << fmt::format("Incorrect test input of {} and {}", entry.a, entry.b);

        auto optionalVariable = instance.evaluateExpression(&equalityExpression, {});
        ASSERT_TRUE(optionalVariable.has_value());
        ASSERT_EQ(optionalVariable->type(), Variable::Type::BOOLEAN);
        EXPECT_EQ(optionalVariable->toBoolean(), entry.shouldPass)
                << (entry.shouldPass
                        ? fmt::format("{} should have been less than {})", entry.a, entry.b)
                        : fmt::format("{} should not have been less than {})", entry.a, entry.b));
    }
}
TEST(Expressions_IntegerComparisons, LessThanOrEqualTo) {
    constexpr const std::array<Entry, 12> entries{{
        {true, 0, 0},
        {true, 628612961, 628612961},
        {false, 1, 0},
        {true, 0, 1},
        {false, std::numeric_limits<std::int64_t>::max(), 0},
        {true, std::numeric_limits<std::int64_t>::min(), 0},
        {true, 0, std::numeric_limits<std::int64_t>::max()},
        {false, 0, std::numeric_limits<std::int64_t>::min()},
        {false, std::numeric_limits<std::int64_t>::max(), std::numeric_limits<std::int64_t>::min()},
        {true, std::numeric_limits<std::int64_t>::min(), std::numeric_limits<std::int64_t>::max()},
        {true, std::numeric_limits<std::int64_t>::min(), std::numeric_limits<std::int64_t>::min()},
        {true, std::numeric_limits<std::int64_t>::max(), std::numeric_limits<std::int64_t>::max()},
    }};
    for (const auto &entry : entries) {
        BiExpression equalityExpression{
            Expression::Type::COMPARE_LESS_THAN_OR_EQUAL,
            std::make_unique<PrimaryExpression<std::int64_t>>(Expression::Type::PRIMARY_INTEGER, entry.a),
            std::make_unique<PrimaryExpression<std::int64_t>>(Expression::Type::PRIMARY_INTEGER, entry.b)
        };

        ASSERT_EQ(entry.a <= entry.b, entry.shouldPass)
            << fmt::format("Incorrect test input of {} and {}", entry.a, entry.b);

        auto optionalVariable = instance.evaluateExpression(&equalityExpression, {});
        ASSERT_TRUE(optionalVariable.has_value());
        ASSERT_EQ(optionalVariable->type(), Variable::Type::BOOLEAN);
        EXPECT_EQ(optionalVariable->toBoolean(), entry.shouldPass)
                << (entry.shouldPass
                        ? fmt::format("{} should have been less than or equal to {})", entry.a, entry.b)
                        : fmt::format("{} should not have been less than or equal to {})", entry.a, entry.b));
    }
}
