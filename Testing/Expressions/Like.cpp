/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Testing/Base.hpp"

#define STRAWBERRY_INTERNAL_VISIBILITY public
#include "Source/Strawberry.hpp"

#include "Source/Expression/LikeExpression.hpp"

using namespace strawberry;

Strawberry instance{kStandardMessageInterface};

struct SimpleEntry {
    std::string_view input;
    bool expectedToMatch;
};

/**
 * The zero-or-more selector '%' should match all possible input (strings).
 */
TEST(Expressions_Like, AllUsingPercentage) {
    for (std::string_view input : {"%", "aatat", "    ", "_t6q25616($%#(Q58236", ""}) {
        LikeExpression expression{TableColumnSpecifier{{}, {"str"}},
                                  {"%"}};
        VariableList variableList{
                {"str"sv, Variable{std::string(input)}},
        };
        auto result = instance.evaluateLikeExpression(&expression, variableList);
        EXPECT_TRUE(result.has_value()) << "String '" << input << "' failed to go through the evaluator";

        if (result.has_value()) {
            EXPECT_TRUE(result.value()) << "String '" << input << "' doesn't match the all selector";
        }
    }
}

TEST(Expressions_Like, AllWithEmptyMatchString) {
    for (std::string_view input : {"%", "aatat", "    ", "_t6q25616($%#(Q58236", ""}) {
        LikeExpression expression{TableColumnSpecifier{{}, {"str"}},
                                  {""}};
        VariableList variableList{
                {"str"sv, Variable{std::string(input)}},
        };
        auto result = instance.evaluateLikeExpression(&expression, variableList);
        EXPECT_TRUE(result.has_value()) << "String '" << input << "' failed to go through the evaluator";

        if (result.has_value()) {
            EXPECT_TRUE(result.value()) << "String '" << input << "' doesn't match the all selector";
        }
    }
}

TEST(Expressions_Like, BeginsWith) {
    constexpr const std::array<SimpleEntry, 5> entries{{
        {"Hello", false},
        {"hello", true},
        {"world", false},
        {"highway", true},
        {"", false}
    }};
    for (const auto &entry : entries) {
        LikeExpression expression{TableColumnSpecifier{{}, {"str"}},
                                  {"h%"}};
        VariableList variableList{
                {"str"sv, Variable{std::string(entry.input)}},
        };
        auto result = instance.evaluateLikeExpression(&expression, variableList);
        EXPECT_TRUE(result.has_value()) << "String '" << entry.input << "' failed to go through the evaluator";
        if (result.has_value()) {
            if (entry.expectedToMatch) {
                EXPECT_TRUE(result.value()) << "String '" << entry.input << "' doesn't match, but should";
            } else {
                EXPECT_FALSE(result.value()) << "String '" << entry.input << "' matches, but shouldn't";
            }
        }
    }
}

TEST(Expressions_Like, ExactMatch) {
    constexpr const std::array<SimpleEntry, 20> entries{{
        {"Hello", false},
        {"hello", true},
        {"hellO", false},
        {" hello", false},
        {" hello ", false},
        {"\nhello", false},
        {"\nhello\n", false},
        {"\thello", false},
        {"\thello\t", false},
        {"world", false},
        {"highway", false},
        {"", false},
        {"hello-with-suffix", false},
        {"prefix-with-hello", false},
        {"hel-with-infix-lo", false},
        {"prefix-with-hello-with-suffix", false},
        {"prefix-with-hel-with-infix-lo-with-suffix", false},
        {"hel-with-infix-lo-with-suffix", false},
        {"prefix-with-hel-with-infix-lo", false},
        {"hello", true},
    }};
    for (const auto &entry : entries) {
        LikeExpression expression{TableColumnSpecifier{{}, {"str"}},
                                  {"hello"}};
        VariableList variableList{
                {"str"sv, Variable{std::string(entry.input)}},
        };
        auto result = instance.evaluateLikeExpression(&expression, variableList);
        EXPECT_TRUE(result.has_value()) << "String '" << entry.input << "' failed to go through the evaluator";
        if (result.has_value()) {
            if (entry.expectedToMatch) {
                EXPECT_TRUE(result.value()) << "String '" << entry.input << "' doesn't match, but should";
            } else {
                EXPECT_FALSE(result.value()) << "String '" << entry.input << "' matches, but shouldn't";
            }
        }
    }
}

TEST(Expressions_Like, EndsWith) {
    constexpr const std::array<SimpleEntry, 9> entries{{
        {"Hello", true},
        {"HEllo", true},
        {"heLLO", false},
        {"HeLlO", false},
        {"hello", true},
        {"not-lld-but-llo", true},
        {"world", false},
        {"highway", false},
        {"", false}
    }};
    for (const auto &entry : entries) {
        LikeExpression expression{TableColumnSpecifier{{}, {"str"}},
                                  {"%llo"}};
        VariableList variableList{
                {"str"sv, Variable{std::string(entry.input)}},
        };
        auto result = instance.evaluateLikeExpression(&expression, variableList);
        EXPECT_TRUE(result.has_value()) << "String '" << entry.input << "' failed to go through the evaluator";
        if (result.has_value()) {
            if (entry.expectedToMatch) {
                EXPECT_TRUE(result.value()) << "String '" << entry.input << "' doesn't match, but should";
            } else {
                EXPECT_FALSE(result.value()) << "String '" << entry.input << "' matches, but shouldn't";
            }
        }
    }
}
