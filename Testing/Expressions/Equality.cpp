/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Testing/Base.hpp"

#include <fmt/format.h>

#define STRAWBERRY_INTERNAL_VISIBILITY public
#include "Source/Strawberry.hpp"

#include "Source/Expression/BiExpression.hpp"
#include "Source/Expression/PrimaryExpression.hpp"

using namespace strawberry;

Strawberry instance{kStandardMessageInterface};

TEST(Expressions_Equality, IntegerVsString) {
    struct Entry {
        bool isPassing;
        std::int64_t integer;
        std::string_view string;
    };
    std::array<Entry, 10> entries{{
        {true, 1, "1"},
        {true, 0, "0000"},
        {true, 629626, "629626"},
        {true, -51285156, "-51285156"},
        {false, -81862, "81862"},
        {false, -10, "-10s"},
        {false, -10, "-10R"},
        {false, 32, "--32"},
        {false, -32, "-Q32"},
        {false, 9023, " -9023"},
    }};
    for (const auto &entry : entries) {
        BiExpression equalityExpression{
            Expression::Type::COMPARE_EQUAL_TO,
            std::make_unique<PrimaryExpression<std::int64_t>>(Expression::Type::PRIMARY_INTEGER, entry.integer),
            std::make_unique<PrimaryExpression<std::string>>(Expression::Type::PRIMARY_STRING, std::string(entry.string))
        };
        auto optionalVariable = instance.evaluateExpression(&equalityExpression, {});
        ASSERT_TRUE(optionalVariable.has_value());
        ASSERT_EQ(optionalVariable->type(), Variable::Type::BOOLEAN);
        EXPECT_EQ(optionalVariable->toBoolean(), entry.isPassing)
                << (entry.isPassing
                        ? fmt::format(R"(integer {} should have been equal to "{}")", entry.integer, entry.string)
                        : fmt::format(R"(integer {} should not have been equal to "{}")", entry.integer, entry.string));
    }
}
