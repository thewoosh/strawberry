/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Testing/Base.hpp"

#include <fmt/format.h>

#define STRAWBERRY_INTERNAL_VISIBILITY public
#include "Source/Strawberry.hpp"

#include "Source/Expression/BiExpression.hpp"
#include "Source/Expression/PrimaryExpression.hpp"

using namespace strawberry;

Strawberry instance{kStandardMessageInterface};

struct Entry {
    bool shouldPass;
    std::string_view a;
    std::string_view b;
};

TEST(Expressions_LexicographicallyStringComparisons, GreaterThan) {
    constexpr const std::array<Entry, 5> entries{{
        {false, "The same string", "The same string"},
        {true, "something", ""}, // something is greater than nothing
        {true, "prefixBsuffix", "prefixAsuffix"},
        {false, "", ""},
        {false, "abcdef", "bcdefg"},
    }};
    for (const auto &entry : entries) {
        BiExpression equalityExpression{
            Expression::Type::COMPARE_GREATER_THAN,
            std::make_unique<PrimaryExpression<std::string>>(Expression::Type::PRIMARY_STRING, std::string(entry.a)),
            std::make_unique<PrimaryExpression<std::string>>(Expression::Type::PRIMARY_STRING, std::string(entry.b))
        };

        ASSERT_EQ(entry.a > entry.b, entry.shouldPass)
            << fmt::format(R"(Incorrect test input of "{}" and "{}")", entry.a, entry.b);

        auto optionalVariable = instance.evaluateExpression(&equalityExpression, {});
        ASSERT_TRUE(optionalVariable.has_value());
        ASSERT_EQ(optionalVariable->type(), Variable::Type::BOOLEAN);
        EXPECT_EQ(optionalVariable->toBoolean(), entry.shouldPass)
                << (entry.shouldPass
                        ? fmt::format(R"("{}" should have been greater than "{}")", entry.a, entry.b)
                        : fmt::format(R"("{}" should not have been greater than "{}")", entry.a, entry.b));
    }
}

TEST(Expressions_LexicographicallyStringComparisons, GreaterThanOrEqualTo) {
    constexpr const std::array<Entry, 5> entries{{
        {true, "The same string", "The same string"},
        {true, "something", ""},
        {true, "prefixBsuffix", "prefixAsuffix"},
        {true, "", ""},
        {false, "abcdef", "bcdefg"},
    }};
    for (const auto &entry : entries) {
        BiExpression equalityExpression{
            Expression::Type::COMPARE_GREATER_THAN_OR_EQUAL,
            std::make_unique<PrimaryExpression<std::string>>(Expression::Type::PRIMARY_STRING, std::string(entry.a)),
            std::make_unique<PrimaryExpression<std::string>>(Expression::Type::PRIMARY_STRING, std::string(entry.b))
        };

        ASSERT_EQ(entry.a >= entry.b, entry.shouldPass)
            << fmt::format(R"(Incorrect test input of "{}" and "{}")", entry.a, entry.b);

        auto optionalVariable = instance.evaluateExpression(&equalityExpression, {});
        ASSERT_TRUE(optionalVariable.has_value());
        ASSERT_EQ(optionalVariable->type(), Variable::Type::BOOLEAN);
        EXPECT_EQ(optionalVariable->toBoolean(), entry.shouldPass)
                << (entry.shouldPass
                        ? fmt::format(R"("{}" should have been greater than or equal to "{}")", entry.a, entry.b)
                        : fmt::format(R"("{}" should not have been greater than or equal to "{}")", entry.a, entry.b));
    }
}

TEST(Expressions_LexicographicallyStringComparisons, EqualTo) {
    constexpr const std::array<Entry, 7> entries{{
        {true, "The same string", "The same string"},
        {false, "prefixBsuffix", "prefixAsuffix"},
        {false, "prefixAsuffix", "prefixBsuffix"},
        {true, "", ""},
        {false, "abcdef", "bcdefg"},
        {false, "something", ""},
        {false, "", "something"}
    }};
    for (const auto &entry : entries) {
        BiExpression equalityExpression{
            Expression::Type::COMPARE_EQUAL_TO,
            std::make_unique<PrimaryExpression<std::string>>(Expression::Type::PRIMARY_STRING, std::string(entry.a)),
            std::make_unique<PrimaryExpression<std::string>>(Expression::Type::PRIMARY_STRING, std::string(entry.b))
        };

        ASSERT_EQ(entry.a == entry.b, entry.shouldPass)
            << fmt::format(R"(Incorrect test input of "{}" and "{}")", entry.a, entry.b);

        auto optionalVariable = instance.evaluateExpression(&equalityExpression, {});
        ASSERT_TRUE(optionalVariable.has_value());
        ASSERT_EQ(optionalVariable->type(), Variable::Type::BOOLEAN);
        EXPECT_EQ(optionalVariable->toBoolean(), entry.shouldPass)
                << (entry.shouldPass
                        ? fmt::format(R"("{}" should have been equal to "{}")", entry.a, entry.b)
                        : fmt::format(R"("{}" should not have been equal to "{}")", entry.a, entry.b));
    }
}

TEST(Expressions_LexicographicallyStringComparisons, LessThan) {
    constexpr const std::array<Entry, 6> entries{{
        {false, "The same string", "The same string"},
        {true, "", "something"}, // nothing is less than something
        {true, "prefixAsuffix", "prefixBsuffix"},
        {false, "", ""},
        {true, "abcdef", "bcdefg"},
        {false, "bcdefg", "abcdef"}
    }};
    for (const auto &entry : entries) {
        BiExpression equalityExpression{
            Expression::Type::COMPARE_LESS_THAN,
            std::make_unique<PrimaryExpression<std::string>>(Expression::Type::PRIMARY_STRING, std::string(entry.a)),
            std::make_unique<PrimaryExpression<std::string>>(Expression::Type::PRIMARY_STRING, std::string(entry.b))
        };

        ASSERT_EQ(entry.a < entry.b, entry.shouldPass)
            << fmt::format(R"(Incorrect test input of "{}" and "{}")", entry.a, entry.b);

        auto optionalVariable = instance.evaluateExpression(&equalityExpression, {});
        ASSERT_TRUE(optionalVariable.has_value());
        ASSERT_EQ(optionalVariable->type(), Variable::Type::BOOLEAN);
        EXPECT_EQ(optionalVariable->toBoolean(), entry.shouldPass)
                << (entry.shouldPass
                        ? fmt::format(R"("{}" should have been less than "{}")", entry.a, entry.b)
                        : fmt::format(R"("{}" should not have been less than "{}")", entry.a, entry.b));
    }
}

TEST(Expressions_LexicographicallyStringComparisons, LessThanOrEqualTo) {
    constexpr const std::array<Entry, 8> entries{{
        {true, "The same string", "The same string"},
        {true, "", "something"}, // nothing is less than something
        {true, "prefixAsuffix", "prefixBsuffix"},
        {true, "", ""},
        {true, "abcdef", "bcdefg"},
        {false, "bcdefg", "abcdef"},
        {false, "something", ""},
        {false, "prefixBsuffix", "prefixAsuffix"},
    }};
    for (const auto &entry : entries) {
        BiExpression equalityExpression{
            Expression::Type::COMPARE_LESS_THAN_OR_EQUAL,
            std::make_unique<PrimaryExpression<std::string>>(Expression::Type::PRIMARY_STRING, std::string(entry.a)),
            std::make_unique<PrimaryExpression<std::string>>(Expression::Type::PRIMARY_STRING, std::string(entry.b))
        };

        ASSERT_EQ(entry.a <= entry.b, entry.shouldPass)
            << fmt::format(R"(Incorrect test input of "{}" and "{}")", entry.a, entry.b);

        auto optionalVariable = instance.evaluateExpression(&equalityExpression, {});
        ASSERT_TRUE(optionalVariable.has_value());
        ASSERT_EQ(optionalVariable->type(), Variable::Type::BOOLEAN);
        EXPECT_EQ(optionalVariable->toBoolean(), entry.shouldPass)
                << (entry.shouldPass
                        ? fmt::format(R"("{}" should have been less than or equal to "{}")", entry.a, entry.b)
                        : fmt::format(R"("{}" should not have been less than or equal to "{}")", entry.a, entry.b));
    }
}
