/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * This work is subject to the terms of a BSD 2 Clause-like license.
 * If the COPYING file was not distributed along this file, you can
 * obtain a license at https://thewoosh.org/Licensing/Strawberry.txt.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <queue>
#include <string>

#include "Source/Interface/MessageInterface.hpp"

namespace strawberry {

    struct MessageSink {
        using Container = std::queue<std::string>;

        [[nodiscard]] MessageInterface
        createInterface() {
            MessageInterface messageInterface{};

            messageInterface.onLexerError = [&] (std::string_view message) {
                m_lexerErrorStorage.emplace(message);
            };

            messageInterface.onParserError = [&] (std::string_view message) {
                m_parserErrorStorage.emplace(message);
            };

            messageInterface.onQueryError = [&] (std::string_view message) {
                m_queryErrorStorage.emplace(message);
            };

            messageInterface.onInformation = [&] (std::string_view message) {
                m_informationStorage.emplace(message);
            };

            return messageInterface;
        }

        inline void
        clear() {
            m_lexerErrorStorage = {};
            m_parserErrorStorage = {};
            m_queryErrorStorage = {};
            m_informationStorage = {};
        }

        [[nodiscard]] Container &
        lexerErrors() {
            return m_lexerErrorStorage;
        }

        [[nodiscard]] Container &
        parserErrors() {
            return m_parserErrorStorage;
        }

        [[nodiscard]] Container &
        queryErrors() {
            return m_queryErrorStorage;
        }

        [[nodiscard]] Container &
        informationMessages() {
            return m_informationStorage;
        }

    private:
        Container m_lexerErrorStorage{};
        Container m_parserErrorStorage{};
        Container m_queryErrorStorage{};
        Container m_informationStorage{};
    };

} // namespace strawberry
