# SPDX-License-Identifier: BSD-2-Clause
#
# Copyright (C) 2021 Tristan Gerritsen.
# All Rights Reserved.

file(GLOB_RECURSE TestSources CONFIGURE_DEPENDS */*.cpp)

foreach(TestFile ${TestSources})
    file(RELATIVE_PATH RelativeName ${CMAKE_CURRENT_SOURCE_DIR} ${TestFile})
    get_filename_component(TestName ${RelativeName} NAME_WE)
    get_filename_component(DirectoryName ${RelativeName} DIRECTORY)

    string(PREPEND TestName ${DirectoryName} "_")
    string(APPEND TestName "_Tests")

    add_executable(${TestName} ${RelativeName})
    set_target_properties(${TestName} PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${DirectoryName})
    target_link_libraries(${TestName} gtest gtest_main StrawberryCore project_diagnostics)

    add_test(NAME ${TestName}
             COMMAND ${TestName})
endforeach()
