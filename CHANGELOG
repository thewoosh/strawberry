Strawberry SQL Changelog
========================
This changelog contains information about notable changes in the Strawberry SQL
programs, utilities, and interfaces.

---------------
Version 2021.10
---------------

* Added support for the SHOW DATABASES query
* Added support for the CREATE TABLE query
* Added support for the SHOW TABLES query
* Added support for the DESCRIBE query
* Added support for the INSERT...INTO query
* Store data inside ColumnContainers internally. This means that the inserted
  SQL data is stored in memory without being discarded. However, this doesn't
  provide a way for saving the databases and tables to disk.
* Added support for the SELECT...FROM query
* All queries now return a table as expected
* Add support for aliasing names (SELECT long_name AS nm) in SELECT...FROM
  queries
* Make sure all substantial parts of Strawberry will exit gracefully when
  errors occur.
* Make Strawberry more API-like by having a separate Message API. This API
  provides a common gateway for errors, warnings and information to reach the
  end-user.
* SHOW DATABASES/TABLES now store the result in the resulting table, instead of
  being displayed as informational messages.
* Added support for a basic WHERE clause in SELECT...FROM queries.
* Add static analysis. This change adds suggestions for the correct column,
  table, database, etc. name.
* Added alias for SHOW DATABASE namely SHOW SCHEMAS.
* Support for SHOW TABLES to provide a database name, whereas by default it
  selects the current database.
* Allow strings in expressions (thus e.g. WHERE clauses).
* Improved the WHERE clause to contain the values of columns that aren't
  explicitly selected by the SELECT ... clause, but are part of the same
  rows of the tables.
* Support string parsing (and internally, string variable support).
* Added the LIKE operator: 'hello world' LIKE 'hello%'
* Added data type precedence ordering to convert types to each other. This also
  improves the variable comparison code.
* Corrected the path of WHERE and non-WHERE path so the WHERE path is the same
  as the non-WHERE path, but just the WHERE invoked separately. This improves
  code quality, also.
* Added ORDER BY clause support in SELECT...FROM. This makes it so you can sort
  a column in the resulting table.
