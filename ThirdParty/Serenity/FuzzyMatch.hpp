/*
 * Copyright (c) 2021, Spencer Dixon <spencercdixon@gmail.com>
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * =========================================================================== *
 *                      Begin of the Serenity OS License                       *
 * =========================================================================== *
 *
 * BSD 2-Clause License
 *
 * Copyright (c) 2018-2021, the SerenityOS developers.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * =========================================================================== *
 *                       End of the Serenity OS License                        *
 * =========================================================================== *
 *
 * This file is from the Serenity OS project, transformed and paraphrased in
 * such a way that is applicable for this, the Strawberry project and conforms
 * to its style guidelines.
 */

#pragma once

#include <cstring> // for std::memcpy

#include "Source/Utility/Characters.hpp"

namespace serenity {

    static constexpr const int kRecursionLimit = 10;
    static constexpr const int kMaxMatches = 256;

    static constexpr const int kSequentialBonus = 15;           // bonus for adjacent matches (needle: 'ca', haystack: 'cat')
    static constexpr const int kSeparatorBonus = 30;            // bonus if match occurs after a separator ('_' or ' ')
    static constexpr const int kCamelBonus = 30;                // bonus if match is uppercase and prev is lower (needle: 'myF' haystack: '/path/to/myFile.txt')
    static constexpr const int kFirstLetterBonus = 20;          // bonus if the first letter is matched (needle: 'c' haystack: 'cat')
    static constexpr const int kLeadingLetterPenalty = -5;      // penalty applied for every letter in str before the first match
    static constexpr const int kMaxLeadingLetterPenalty = -15;  // maximum penalty for leading letters
    static constexpr const int kUnmatchedLetterPenalty = -1;    // penalty for every letter that doesn't matter

    static constexpr const std::size_t kRecursiveMatchLimit = 256;

    struct FuzzyMatchResult {
        bool matched;
        int score;
    };

    [[nodiscard]] inline static constexpr FuzzyMatchResult
    fuzzyMatchRecursive(std::string_view needle, std::string_view haystack, size_t needleIndex, size_t haystackIndex,
                        const std::uint8_t *sourceMatches, std::uint8_t *matches, std::size_t nextMatch,
                        int &recursionCount) {
        int finalScore = 0;

        ++recursionCount;
        if (recursionCount >= kRecursionLimit)
            return {false, finalScore};

        if (needle.length() == needleIndex || haystack.length() == haystackIndex)
            return {false, finalScore};

        bool hadRecursiveMatch = false;
        std::array<std::uint8_t, kRecursiveMatchLimit> bestRecursiveMatches{};
        int bestRecursiveScore = 0;

        bool isFirstMatch = true;
        while (needleIndex < needle.length() && haystackIndex < haystack.length()) {
            if (strawberry::toASCIILowercase(needle[needleIndex]) == strawberry::toASCIILowercase(haystack[haystackIndex])) {
                if (nextMatch >= kMaxMatches)
                    return {false, finalScore };

                if (isFirstMatch && sourceMatches) {
                    std::memcpy(matches, sourceMatches, static_cast<std::size_t>(nextMatch));
                    isFirstMatch = false;
                }

                std::array<std::uint8_t, kRecursiveMatchLimit> recursiveMatches{};
                auto result = fuzzyMatchRecursive(needle, haystack, needleIndex, haystackIndex + 1, matches,
                                                  std::data(recursiveMatches), nextMatch, recursionCount);
                if (result.matched) {
                    if (!hadRecursiveMatch || result.score > bestRecursiveScore) {
                        std::memcpy(std::data(bestRecursiveMatches), std::data(recursiveMatches), kRecursiveMatchLimit);
                        bestRecursiveScore = result.score;
                    }
                    hadRecursiveMatch = true;
                    matches[nextMatch++] = static_cast<std::uint8_t>(haystackIndex);
                }
                needleIndex++;
            }
            haystackIndex++;
        }

        if (needleIndex == needle.length()) {
            finalScore = 100;

            int penalty = kLeadingLetterPenalty * matches[0];
            if (penalty < kMaxLeadingLetterPenalty)
                penalty = kMaxLeadingLetterPenalty;
            finalScore += penalty;

            auto unmatched = static_cast<int>(haystack.length() - static_cast<std::size_t>(nextMatch));
            finalScore += kUnmatchedLetterPenalty * unmatched;

            for (std::size_t i = 0; i < nextMatch; i++) {
                const auto currentIndex = matches[i];

                if (i > 0 && currentIndex == matches[i - 1])
                    finalScore += kSequentialBonus;

                if (currentIndex > 0) {
                    std::uint32_t currentCharacter = static_cast<unsigned char>(haystack[currentIndex]);
                    std::uint32_t neighborCharacter = static_cast<unsigned char>(haystack[currentIndex - 1]);

                    if (neighborCharacter != strawberry::toASCIIUppercase(neighborCharacter)
                        && currentCharacter != strawberry::toASCIILowercase(currentCharacter))
                        finalScore += kCamelBonus;

                    if (neighborCharacter == '_' || neighborCharacter == ' ')
                        finalScore += kSeparatorBonus;
                } else {
                    finalScore += kFirstLetterBonus;
                }
            }

            if (hadRecursiveMatch && bestRecursiveScore > finalScore) {
                std::memcpy(matches, std::data(bestRecursiveMatches), kMaxMatches);
                finalScore = bestRecursiveScore;
            }

            return {true, finalScore};
        }

        return {false, finalScore};
    }

    [[nodiscard]] inline constexpr FuzzyMatchResult
    fuzzyMatch(std::string_view needle, std::string_view haystack) {
        int recursion_count = 0;
        std::array<std::uint8_t, kMaxMatches> matches{};
        return fuzzyMatchRecursive(needle, haystack, 0, 0, nullptr, std::data(matches), 0, recursion_count);
    }

} // namespace serenity